package com.pig.easy.bpm.vo.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/7/2 20:50
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
public class DictItemQueryVO implements Serializable {

    private static final long serialVersionUID = 2607784059020890655L;

    @NotNull
    private Long dictId;

    private String itemValue;

    @NotNull
    private String tenantId;

    private String itemText;

    private Integer sort;

    private String remark;

    private Integer validState;
}
