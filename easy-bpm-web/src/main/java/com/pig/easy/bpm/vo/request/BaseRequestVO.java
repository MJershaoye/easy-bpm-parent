package com.pig.easy.bpm.vo.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/5/21 10:37
 */
@Data
@ToString
public class BaseRequestVO implements Serializable {

    private static final long serialVersionUID = 4864439064571263140L;

    @NotNull
    @NotEmpty
    @ApiModelProperty("租户号")
    protected String tenantId;

    @NotNull
    @NotEmpty
    @ApiModelProperty("来源系统")
    protected String system;

    @NotNull
    @NotEmpty
    @ApiModelProperty("来源平台")
    protected String paltform;
}
