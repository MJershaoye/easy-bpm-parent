package com.pig.easy.bpm.config;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * todo: swagger 配置 可放入
 *
 * @author : pig
 * @date : 2020/5/15 14:00
 */
@Configuration
@EnableSwagger2
@Profile({"dev", "local", "pre", "test", "prod"})
public class SwaggerConfig {

    @NacosValue(value = "${swagger.controller:}", autoRefreshed = true)
    private String controller;
    @NacosValue(value = "${swagger.title:}", autoRefreshed = true)
    private String title;
    @NacosValue(value = "${swagger.description:}", autoRefreshed = true)
    private String description;
    @NacosValue(value = "${swagger.version:}", autoRefreshed = true)
    private String version;
    @NacosValue(value = "${swagger.license:}", autoRefreshed = true)
    private String license;
    @NacosValue(value = "${swagger.licenseUrl:}", autoRefreshed = true)
    private String licenseUrl;
    @NacosValue(value = "${swagger.author:}", autoRefreshed = true)
    private String author;
    @NacosValue(value = "${swagger.authorBlogUrl:}", autoRefreshed = true)
    private String authorBlogUrl;
    @NacosValue(value = "${swagger.email:}", autoRefreshed = true)
    private String email;

    @Bean
    public Docket createRestApi() {
        checkData();
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(controller))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(new ApiInfoBuilder()
                        .title(title)
                        .description(description)
                        .version(version)
                        .license(license)
                        .licenseUrl(licenseUrl)
                        .contact(new Contact(author, authorBlogUrl, email))
                        .build());
    }


    private void checkData() {
        if (StringUtils.isEmpty(controller) || StringUtils.isEmpty(title)) {
            throw new RuntimeException("SwaggerConfig init fail, please config on nacos");
        }
        System.out.println("SwaggerConfig  ############################## = " + title);
    }
}
