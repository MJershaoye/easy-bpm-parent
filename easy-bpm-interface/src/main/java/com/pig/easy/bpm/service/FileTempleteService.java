package com.pig.easy.bpm.service;

import com.pig.easy.bpm.dto.request.*;
import com.pig.easy.bpm.dto.response.*;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.utils.Result;

import java.util.List;

/**
 * <p>
 * 模板文件表 服务类
 * </p>
 *
 * @author pig
 * @since 2020-08-10
 */
public interface FileTempleteService {

    Result<PageInfo<FileTempleteDTO>> getListByCondition(FileTempleteQueryDTO param);

    Result<List<FileTempleteDTO>> getListByProcessIdAndTenantId(String tenantId, Long processId);

    Result<Integer> insertFileTemplete(FileTempleteSaveOrUpdateDTO param);

    Result<Integer> updateFileTemplete(FileTempleteSaveOrUpdateDTO param);

    Result<Integer> deleteFileTemplete(FileTempleteSaveOrUpdateDTO param);

}
