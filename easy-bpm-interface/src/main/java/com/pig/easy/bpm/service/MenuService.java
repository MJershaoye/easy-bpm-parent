package com.pig.easy.bpm.service;

import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.MenuQueryDTO;
import com.pig.easy.bpm.dto.request.MenuSaveOrUpdateDTO;
import com.pig.easy.bpm.dto.response.MenuDTO;
import com.pig.easy.bpm.dto.response.MenuTreeDTO;
import com.pig.easy.bpm.utils.Result;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pig
 * @since 2020-07-09
 */
public interface MenuService {

        Result<PageInfo<MenuDTO>> getListByCondition(MenuQueryDTO param);

        Result<List<MenuTreeDTO>> getMenuTree(String tenantId, Long parentId);

        Result<Integer> insertMenu(MenuSaveOrUpdateDTO param);

        Result<Integer> updateMenu(MenuSaveOrUpdateDTO param);

        Result<Integer> deleteMenu(MenuSaveOrUpdateDTO param);

 }
