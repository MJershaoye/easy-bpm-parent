package com.pig.easy.bpm.dto.response;

import lombok.Data;
import lombok.ToString;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/5/28 15:00
 */
@Data
@ToString
public class FormDTO extends BaseResponseDTO {

    private static final long serialVersionUID = 4758621044459590180L;

    private Long formId;

    private String formKey;

    private String formName;

    private String tenantId;

    private Integer formType;

    private Integer sort;

    private String formData;

    private String remarks;

    private Integer validState;

    private Long operatorId;

    private String operatorName;

}
