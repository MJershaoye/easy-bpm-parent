package com.pig.easy.bpm.service;

import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.ProcessDetailReqDTO;
import com.pig.easy.bpm.dto.request.ProcessPublishDTO;
import com.pig.easy.bpm.dto.response.ProcessDetailDTO;
import com.pig.easy.bpm.utils.Result;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author pig
 * @since 2020-06-06
 */
public interface ProcessDetailService {

    Result<ProcessDetailDTO> getProcessDetailByProcessId(Long processId);

    Result<ProcessDetailDTO> getProcessDetailByDefinitionId(String definitionId);

    Result<ProcessDetailDTO> getProcessDetailById(Long processDetailId);

    Result<PageInfo> getListByCondiction(ProcessDetailReqDTO processDetailReqDTO);

    Result<Integer> updateProcessDetail(ProcessDetailReqDTO processDetailReqDTO);

    Result<Integer> insertProcessDetail(ProcessDetailReqDTO processDetailReqDTO);

    Result<Boolean> checkProcessXml(String processXml);

    Result<Boolean> processPublish(ProcessPublishDTO processPublishDTO);

    Result<Boolean> updateDefaultVersion(Long processId, Long processDetailId, Long operatorId, String operatorName);

    Result<Boolean> saveNodeList(String processXml, String definitionId, String processKey, Long operatorId, String operatorName);

}
