package com.pig.easy.bpm.service;

import com.pig.easy.bpm.dto.request.HistorySaveDTO;
import com.pig.easy.bpm.dto.response.HistoryDTO;
import com.pig.easy.bpm.utils.Result;

import java.util.List;

/**
 * <p>
 * 审批历史表 服务类
 * </p>
 *
 * @author pig
 * @since 2020-07-01
 */
public interface HistoryService {

    Result<List<HistoryDTO>> getListByApplyId(Long applyId);

    Result<HistoryDTO> getHistoryByTaskId(Long taskId);

    Result<Integer> addHistory(HistorySaveDTO historySaveDTO);

    Result<Integer> updateHistoryById(HistorySaveDTO historySaveDTO);

    Result<Integer> insertHistory(Long applyId, String tenantId, Long taskId, String taskName, Long approverUserId, String approveRealName, String approveActionCode, String approveOpinion, String systemCode, String paltform);

}
