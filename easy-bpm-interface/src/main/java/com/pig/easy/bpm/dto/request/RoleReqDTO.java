package com.pig.easy.bpm.dto.request;

import lombok.*;

import java.util.List;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/14 10:48
 */
@ToString
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoleReqDTO extends BaseRequestDTO {

    private static final long serialVersionUID = 4119278940105998696L;

    private Long roleId;

    private List<Long> roleIds;

    private String roleCode;

    private String roleName;

    private Long operatorId;

    private String operatorName;

    private Integer pageSize;

    private Integer pageIndex;
}
