package com.pig.easy.bpm.dto.response;

import lombok.Data;
import lombok.ToString;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/5/21 13:53
 */
@Data
@ToString
public class ConfigTemplateDTO extends BaseResponseDTO {

    private static final long serialVersionUID = -7688188213474147509L;

    private Long templateId;

    private String templateCode;

    private String templateName;

    private String templateKey;

    private String templateValue;

    private Integer templateStatus;

    private String templateType;

    private String remarks;

    private Long operatorId;

    private String operatorName;
}
