package com.pig.easy.bpm.dto.response;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/27 16:16
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
public class TreeDTO extends BaseResponseDTO {

    private static final long serialVersionUID = -4921659616060082663L;

    private String id;

    private Long treeId;

    private String treeCode;

    private String treeName;

    private Integer treeType;

    private String treeTypeCode;

    private Long parentId;

    private Long tempTreeId;

    private List<TreeDTO> children;

}
