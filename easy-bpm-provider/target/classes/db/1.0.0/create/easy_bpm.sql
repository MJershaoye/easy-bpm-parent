/*
Navicat MySQL Data Transfer

Source Server         : zzlAliYun
Source Server Version : 50729
Source Host           :
Source Database       : easy_bpm

Target Server Type    : MYSQL
Target Server Version : 50729
File Encoding         : 65001

Date: 2020-09-02 09:30:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bpm_apply
-- ----------------------------
DROP TABLE IF EXISTS `bpm_apply`;
CREATE TABLE `bpm_apply` (
  `apply_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `apply_sn` varchar(255) NOT NULL DEFAULT '' COMMENT '单据编号',
  `proc_inst_id` varchar(64) NOT NULL DEFAULT '' COMMENT '流程实例编号',
  `apply_title` varchar(255) NOT NULL DEFAULT '' COMMENT '申请标题',
  `apply_user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '申请人工号',
  `apply_real_name` varchar(128) NOT NULL DEFAULT '' COMMENT '申请人姓名',
  `apply_dept_id` bigint(20) NOT NULL COMMENT '部门编号',
  `apply_dept_code` varchar(255) NOT NULL DEFAULT '' COMMENT '申请部门工号',
  `apply_dept_name` varchar(255) NOT NULL DEFAULT '' COMMENT '申请部门名称',
  `apply_company_id` bigint(20) NOT NULL COMMENT '申请人公司编号',
  `apply_company_code` varchar(255) NOT NULL DEFAULT '' COMMENT '申请人公司编码',
  `apply_company_name` varchar(255) NOT NULL DEFAULT '' COMMENT '申请人公司名称',
  `tenant_id` varchar(100) NOT NULL DEFAULT '' COMMENT '租户编号',
  `process_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '流程编号',
  `process_key` varchar(64) NOT NULL DEFAULT '' COMMENT '流程编码',
  `process_name` varchar(255) NOT NULL DEFAULT '' COMMENT '流程名称',
  `definition_id` varchar(128) NOT NULL DEFAULT '' COMMENT '发起流程版本ID',
  `form_key` varchar(64) NOT NULL DEFAULT '' COMMENT '表单KEY',
  `system` varchar(64) NOT NULL DEFAULT '' COMMENT '来源系统',
  `paltform` varchar(64) NOT NULL DEFAULT '' COMMENT '来源编码',
  `parent_apply_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '父级申请编码',
  `apply_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '流程状态 1 草稿 1.0.1 审批中 3 审批通过 4 审批拒绝  5 已取消',
  `pre_process_link` varchar(1000) NOT NULL DEFAULT '' COMMENT '演算审批链',
  `complete_time` datetime DEFAULT NULL COMMENT '审批完成时间',
  `remarks` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `valid_state` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '操作人工号',
  `operator_name` varchar(128) NOT NULL DEFAULT '' COMMENT '操作人工姓名',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '申请时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`apply_id`),
  KEY `idx_apply_sn` (`apply_sn`),
  KEY `idx_proc_inst_id` (`proc_inst_id`),
  KEY `idx_apply_name` (`apply_user_id`,`apply_real_name`),
  KEY `idx_dept_id` (`apply_dept_id`),
  KEY `idx_company_id` (`apply_company_id`),
  KEY `idx_tenant` (`tenant_id`),
  KEY `idx_process_id` (`process_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='申请表';

-- ----------------------------
-- Table structure for bpm_company
-- ----------------------------
DROP TABLE IF EXISTS `bpm_company`;
CREATE TABLE `bpm_company` (
  `company_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '公司编码',
  `company_code` varchar(64) DEFAULT '' COMMENT '部门编码',
  `company_parent_id` bigint(20) DEFAULT '0' COMMENT '上级公司编号',
  `company_parent_code` varchar(64) DEFAULT '' COMMENT '上级公司编码',
  `company_name` varchar(128) DEFAULT '' COMMENT '公司名称',
  `company_abbr` varchar(64) DEFAULT '' COMMENT '公司简称',
  `company_level` tinyint(3) unsigned DEFAULT '1' COMMENT '公司承继',
  `company_order` smallint(5) DEFAULT '1' COMMENT '排序',
  `company_icon` varchar(255) DEFAULT '' COMMENT '公司展示图标',
  `company_url` varchar(255) DEFAULT '' COMMENT '公司展示url',
  `tenant_id` varchar(64) NOT NULL DEFAULT '' COMMENT '租户编号',
  `company_status` tinyint(3) DEFAULT '1' COMMENT '1 正常开业  1.0.1 拟筹 1.0.1 已关停',
  `valid_state` tinyint(2) unsigned DEFAULT '1' COMMENT '有效状态；0表示无效，1表示有效',
  `operator_id` bigint(20) DEFAULT '0' COMMENT '操作人工号',
  `operator_name` varchar(255) DEFAULT '' COMMENT '操作人姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`company_id`),
  UNIQUE KEY `uniq_company_code` (`company_code`),
  KEY `idx_tenant` (`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='公司表';

-- ----------------------------
-- Records of bpm_company
-- ----------------------------
INSERT INTO `bpm_company` VALUES ('1', 'pig:company:testCompany', '0', '', '测试集团', 'test', '1', '1', '', '', 'pig', '1', '1', '0', '', '2020-05-19 09:26:10', '2020-06-24 13:53:10');
INSERT INTO `bpm_company` VALUES ('1.0.1', 'pig:company:bondCompany', '1', 'pig:company:testCompany', '测试证券投资有限公司', 'bond', '1', '1', '', '', 'pig', '1', '1', '0', '', '2020-06-24 11:29:50', '2020-06-24 13:53:22');
INSERT INTO `bpm_company` VALUES ('3', 'pig:company:bankCompany', '1', 'pig:company:testCompany', '测试银行有限公司', 'bank', '1', '1', '', '', 'pig', '1', '1', '0', '', '2020-06-24 11:29:50', '2020-06-24 13:53:23');
INSERT INTO `bpm_company` VALUES ('4', 'pig:company:gdBranchCompany', '1', 'pig:company:testCompany', '测试集团广东分公司', 'gdBranch', '1', '1', '', '', 'pig', '1', '1', '0', '', '2020-06-24 11:33:46', '2020-07-30 14:29:41');

-- ----------------------------
-- Table structure for bpm_config
-- ----------------------------
DROP TABLE IF EXISTS `bpm_config`;
CREATE TABLE `bpm_config` (
  `config_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置编码',
  `config_code` varchar(64) DEFAULT '' COMMENT '模板编号',
  `config_name` varchar(255) DEFAULT '' COMMENT '模板名称',
  `template_id` bigint(20) unsigned DEFAULT NULL COMMENT '模板编号',
  `config_key` varchar(64) DEFAULT '' COMMENT '模板key',
  `config_value` varchar(255) DEFAULT '' COMMENT '模板值',
  `config_type` varchar(255) DEFAULT '' COMMENT '模板类型',
  `tenant_id` varchar(128) DEFAULT '' COMMENT '租户编号',
  `remarks` varchar(255) DEFAULT '' COMMENT '备注',
  `valid_state` tinyint(3) unsigned DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` bigint(20) unsigned DEFAULT NULL COMMENT '操作人工号',
  `operator_name` varchar(255) DEFAULT '' COMMENT '操作人姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`config_id`),
  UNIQUE KEY `uniq_code` (`config_code`) USING BTREE,
  KEY `idx_tenant` (`template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bpm_config
-- ----------------------------
INSERT INTO `bpm_config` VALUES ('1', 'config:pig:tenantId', '租户名称', '1', 'tenantId', 'pig', 'string', 'pig', '', '1', null, '', '2020-06-26 10:17:02', '2020-06-26 10:17:02');
INSERT INTO `bpm_config` VALUES ('1.0.1', 'config:pig:system', '来源系统', '1.0.1', 'system', 'bestBpmAdmin', 'string', 'pig', '', '1', null, '', '2020-06-26 10:17:45', '2020-06-26 10:17:45');
INSERT INTO `bpm_config` VALUES ('3', 'config:pig:paltform', '来源平台', '3', 'paltform', 'pc', 'string', 'pig', '', '1', null, '', '2020-06-26 10:17:48', '2020-06-26 10:17:48');
INSERT INTO `bpm_config` VALUES ('4', 'config:pig:enableNode', '是否启用node表', '4', 'enableNode', '0', 'boolean', 'pig', '', '1', null, '', '2020-06-26 10:17:50', '2020-06-26 10:17:50');
INSERT INTO `bpm_config` VALUES ('5', 'config:pig:flowAdmin', '流程管理员', '5', 'flowAdmin', '1.0.1,3,4', 'string', 'pig', '', '1', null, '', '2020-06-26 10:19:52', '2020-06-26 10:19:52');
INSERT INTO `bpm_config` VALUES ('6', 'config:pig:approvedNodeFill', '查看流程图时，已审批通过节点颜色填充色', '6', 'approvedNodeFill', 'white', 'string', 'pig', '', '1', null, '', '2020-07-05 18:00:20', '2020-07-05 18:00:20');
INSERT INTO `bpm_config` VALUES ('7', 'config:pig:approvedNodeStroke', '查看流程图时，已审批通过节点颜色填充色', '7', 'approvedNodeStroke', 'black', 'string', 'pig', '', '1', null, '', '2020-07-05 18:01:11', '2020-07-05 18:01:11');
INSERT INTO `bpm_config` VALUES ('8', 'config:pig:approvedNodeStrokenWidth', '查看流程图时，已审批通过节点颜色填充色', '8', 'approvedNodeStrokenWidth', '1.0.1', 'string', 'pig', '', '1', null, '', '2020-07-05 18:01:23', '2020-07-05 18:01:23');

-- ----------------------------
-- Table structure for bpm_config_template
-- ----------------------------
DROP TABLE IF EXISTS `bpm_config_template`;
CREATE TABLE `bpm_config_template` (
  `template_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '模板编号',
  `template_code` varchar(64) DEFAULT '' COMMENT '模板编号',
  `template_name` varchar(255) DEFAULT '' COMMENT '模板名称',
  `template_key` varchar(64) DEFAULT '' COMMENT '模板key',
  `template_value` varchar(255) DEFAULT '' COMMENT '模板值',
  `template_type` varchar(255) DEFAULT '' COMMENT '模板类型',
  `template_status` tinyint(2) unsigned DEFAULT '1' COMMENT '模板字段状态 1 未发布 1.0.1 已发布',
  `remarks` varchar(255) DEFAULT '' COMMENT '备注',
  `valid_state` tinyint(3) unsigned DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` bigint(20) unsigned DEFAULT NULL COMMENT '操作人工号',
  `operator_name` varchar(255) DEFAULT '' COMMENT '操作人姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`template_id`),
  UNIQUE KEY `uniq_code` (`template_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bpm_config_template
-- ----------------------------
INSERT INTO `bpm_config_template` VALUES ('1', 'template:tenantId', '默认租户名称', 'tenantId', 'pig', 'string', '1', '默认租户名称', '1', null, '', '2020-05-21 03:14:44', '2020-05-21 03:14:44');
INSERT INTO `bpm_config_template` VALUES ('1.0.1', 'template:system', '默认来源系统', 'system', 'bestBpmAdmin', 'string', '1', '默认来源系统', '1', null, '', '2020-05-21 03:22:00', '2020-05-21 03:22:00');
INSERT INTO `bpm_config_template` VALUES ('3', 'template:paltForm', '默认来源平台', 'paltform', 'pc', 'string', '1', '默认来源平台', '1', null, '', '2020-05-21 07:15:38', '2020-05-21 07:15:38');
INSERT INTO `bpm_config_template` VALUES ('4', 'template:enableNode', '是否启用node表', 'node', '0', 'boolean', '1', '默认不启用', '1', null, '', '2020-06-19 06:17:52', '2020-06-19 06:17:52');
INSERT INTO `bpm_config_template` VALUES ('5', 'template:flowAdmin', '默认流程管理员', 'flowAdmin', '1.0.1,3,4', 'string', '1', '默认流程管理员', '1', null, '', '2020-06-26 10:20:05', '2020-06-26 10:20:05');
INSERT INTO `bpm_config_template` VALUES ('6', 'template:approvedNodeFill', '查看流程图时，已审批通过节点颜色填充色', 'approvedNodeFill', 'white', 'string', '1', '查看流程图时，已审批通过节点颜色填充色', '1', null, '', '2020-06-26 10:20:05', '2020-06-26 10:20:05');
INSERT INTO `bpm_config_template` VALUES ('7', 'template:approvedNodeStroke', '查看流程图时，已审批通过节点边框颜色', 'approvedNodeStrok', 'black', 'string', '1', '查看流程图时，已审批通过节点边框颜色', '1', null, '', '2020-07-05 17:58:07', '2020-07-05 17:58:07');
INSERT INTO `bpm_config_template` VALUES ('8', 'template:approvedNodeStrokenWidth', '查看流程图时，已审批通过节点边框宽度', 'approvedNodeStrok', '1.0.1', 'integer', '1', '查看流程图时，已审批通过节点边框宽度', '1', null, '', '2020-07-05 17:58:07', '2020-07-05 17:58:07');

-- ----------------------------
-- Table structure for bpm_dept
-- ----------------------------
DROP TABLE IF EXISTS `bpm_dept`;
CREATE TABLE `bpm_dept` (
  `dept_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '部门编号',
  `dept_code` varchar(64) NOT NULL DEFAULT '' COMMENT '部门编码',
  `dept_name` varchar(128) NOT NULL DEFAULT '' COMMENT '部门名称',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '所属公司编码',
  `company_code` varchar(64) NOT NULL DEFAULT '' COMMENT '所属公司编码',
  `business_line` varchar(64) NOT NULL DEFAULT '' COMMENT '归属业务条线',
  `tenant_id` varchar(100) NOT NULL DEFAULT '' COMMENT '所属租户',
  `dept_parent_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '上级部门编号',
  `dept_parent_code` varchar(64) NOT NULL DEFAULT '' COMMENT '上级部门编码',
  `dept_level` tinyint(3) NOT NULL DEFAULT '1' COMMENT '部门层级',
  `dept_type` varchar(64) NOT NULL DEFAULT '' COMMENT '部门类型',
  `dept_type_code` varchar(64) NOT NULL DEFAULT '' COMMENT '部门类型编码',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `dept_order` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT '排序',
  `valid_state` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` bigint(20) DEFAULT '0' COMMENT '操作人工号',
  `operator_name` varchar(255) DEFAULT '' COMMENT '操作人姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`dept_id`),
  UNIQUE KEY `uniq_code` (`dept_code`),
  KEY `idx_tenant` (`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='部门表';

-- ----------------------------
-- Records of bpm_dept
-- ----------------------------
INSERT INTO `bpm_dept` VALUES ('1', 'pig:dept:testCompany:tech', '总公司信息技术部', '1', 'pig:company:testCompany', 'tech', 'pig', '0', '', '1', '', '', '', '1', '1', '0', '', '2020-05-19 11:56:02', '2020-06-24 16:18:51');
INSERT INTO `bpm_dept` VALUES ('1.0.1', 'pig:dept:testCompany:generalManagerOffice', '总公司总经理室', '1', 'pig:company:testCompany', 'manager', 'pig', '0', '', '1', '', '', '', '1', '1', '0', '', '2020-06-24 11:39:04', '2020-06-24 16:18:57');
INSERT INTO `bpm_dept` VALUES ('3', 'pig:dept:testCompany:finance', '总公司财务部', '1', 'pig:company:testCompany', 'finance', 'pig', '0', '', '1', '', '', '', '1', '1', '0', '', '2020-06-24 11:39:04', '2020-06-24 16:19:05');
INSERT INTO `bpm_dept` VALUES ('4', 'pig:dept:gdBranchCompany:tech', '广东分公司信息技术部', '4', 'pig:company:gdBranchCompany', 'tech', 'pig', '0', '', '1', '', '', '', '1', '1', '0', '', '2020-05-19 11:56:02', '2020-06-24 16:19:08');
INSERT INTO `bpm_dept` VALUES ('5', 'pig:dept:gdBranchCompany:generalManagerOffice', '广东分公司总经理室', '4', 'pig:company:gdBranchCompany', 'manager', 'pig', '0', '', '1', '', '', '', '1', '1', '0', '', '2020-06-24 11:39:04', '2020-06-24 16:19:14');
INSERT INTO `bpm_dept` VALUES ('6', 'pig:dept:gdBranchCompany:finance', '广东分公司财务部', '4', 'pig:company:gdBranchCompany', 'finance', 'pig', '0', '', '1', '', '', '', '1', '1', '0', '', '2020-06-24 11:39:04', '2020-06-24 16:19:19');
INSERT INTO `bpm_dept` VALUES ('7', 'pig:dept:testCompany:tech:process', '流程管理室', '1', 'pig:company:testCompany', 'tech', 'pig', '1', '', '1', '', '', '', '1', '1', '0', '', '2020-05-19 11:56:02', '2020-07-31 10:59:52');

-- ----------------------------
-- Table structure for bpm_dict
-- ----------------------------
DROP TABLE IF EXISTS `bpm_dict`;
CREATE TABLE `bpm_dict` (
  `dict_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dict_code` varchar(64) NOT NULL DEFAULT '' COMMENT '字典编码',
  `dict_name` varchar(100) NOT NULL DEFAULT '' COMMENT '字典名称',
  `tenant_id` varchar(100) NOT NULL DEFAULT '' COMMENT '租户编号',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `valid_state` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` bigint(20) unsigned DEFAULT '0' COMMENT '操作人工号',
  `operator_name` varchar(255) DEFAULT '' COMMENT '操作人姓名',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`dict_id`),
  UNIQUE KEY `uniq_dict_code` (`dict_code`) USING BTREE,
  KEY `idx_tenant` (`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='字典表';

-- ----------------------------
-- Records of bpm_dict
-- ----------------------------
INSERT INTO `bpm_dict` VALUES ('1', 'pig:dict:validState', '状态', 'pig', '状态', '1', '0', 'admin', '2020-06-27 12:06:03', '2020-06-27 11:11:27');
INSERT INTO `bpm_dict` VALUES ('1.0.1', 'pig:dict:processStatus', '流程状态', 'pig', '流程状态', '1', '0', 'admin', '2020-06-27 20:39:56', '2020-06-27 20:39:56');
INSERT INTO `bpm_dict` VALUES ('3', 'pig:dict:ada', 'das', 'pig', 'sdas', '1', '1.0.1', 'admin', '2020-07-02 18:35:35', '2020-07-02 18:28:24');
INSERT INTO `bpm_dict` VALUES ('4', 'pig:dict:ada1', 'das1', 'pig', 'sdas', '1', '1.0.1', 'admin', '2020-07-02 18:35:36', '2020-07-02 18:29:58');
INSERT INTO `bpm_dict` VALUES ('5', 'pig:dict:gdgsdgds', 'dfdf', 'pig', 'dgd', '1', '1.0.1', 'admin', '2020-07-02 18:35:37', '2020-07-02 18:32:34');
INSERT INTO `bpm_dict` VALUES ('6', 'pig:dict:tryrteyry', 'trr', 'pig', 'ryrtyer', '1', '1.0.1', 'admin', '2020-07-02 18:35:38', '2020-07-02 18:33:37');
INSERT INTO `bpm_dict` VALUES ('7', 'pig:dict:YUTUTYU', 'YTUT', 'pig', 'TUTYUT', '1', '1.0.1', '管理员', '2020-07-02 20:32:41', '2020-07-02 18:35:04');
INSERT INTO `bpm_dict` VALUES ('8', 'pig:dict:nodeAction', '审批动作', 'pig', '审批动作', '1', '1.0.1', '管理员', '2020-08-15 00:04:43', '2020-08-03 17:39:25');

-- ----------------------------
-- Table structure for bpm_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `bpm_dict_item`;
CREATE TABLE `bpm_dict_item` (
  `item_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `dict_id` bigint(20) unsigned NOT NULL COMMENT '字典编号',
  `item_value` varchar(100) NOT NULL DEFAULT '' COMMENT '字典项值',
  `item_text` varchar(100) NOT NULL DEFAULT '' COMMENT '字典项文本',
  `tenant_id` varchar(64) NOT NULL DEFAULT '' COMMENT '租户编号',
  `sort` mediumint(8) unsigned NOT NULL DEFAULT '1' COMMENT '排序',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `valid_state` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '有效状态；0表示无效，1表示有效',
  `operator_id` bigint(20) unsigned DEFAULT NULL COMMENT '操作人工号',
  `operator_name` varchar(100) DEFAULT '' COMMENT '操作人姓名',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`item_id`),
  KEY `idx_dict_id` (`dict_id`),
  KEY `idx_tenant` (`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COMMENT='字典详细表';

-- ----------------------------
-- Records of bpm_dict_item
-- ----------------------------
INSERT INTO `bpm_dict_item` VALUES ('1', '1', '1', '正常', 'pig', '1', '1', '1', '1.0.1', 'admin', '2020-07-02 21:41:20', '2020-07-02 21:41:20');
INSERT INTO `bpm_dict_item` VALUES ('1.0.1', '1', '0', '停用', 'pig', '1', '1', '1', '1.0.1', 'admin', '2020-07-02 21:41:21', '2020-07-02 21:41:21');
INSERT INTO `bpm_dict_item` VALUES ('3', '1.0.1', '1', '草稿', 'pig', '1', '', '1', null, '', '2020-06-27 20:40:43', '2020-06-27 20:40:43');
INSERT INTO `bpm_dict_item` VALUES ('4', '1.0.1', '1.0.1', '审批中', 'pig', '1', '', '1', null, '', '2020-06-27 20:40:59', '2020-06-27 20:40:59');
INSERT INTO `bpm_dict_item` VALUES ('5', '1.0.1', '3', '审批通过', 'pig', '1', '', '1', null, '', '2020-06-27 20:41:17', '2020-06-27 20:41:17');
INSERT INTO `bpm_dict_item` VALUES ('6', '1.0.1', '4', '审批拒绝', 'pig', '1', '', '1', null, '', '2020-06-27 20:42:13', '2020-06-27 20:42:13');
INSERT INTO `bpm_dict_item` VALUES ('7', '1.0.1', '5', '已取消', 'pig', '1', '', '1', null, '', '2020-06-27 20:43:13', '2020-06-27 20:43:13');
INSERT INTO `bpm_dict_item` VALUES ('8', '7', '1', '1', 'pig', '1', '1', '0', '1.0.1', '管理员', '2020-07-02 22:18:37', '2020-07-02 22:18:37');
INSERT INTO `bpm_dict_item` VALUES ('9', '7', '1', '1.0.1', 'pig', '1', '发人人', '0', '1.0.1', '管理员', '2020-07-02 22:18:48', '2020-07-02 22:18:48');
INSERT INTO `bpm_dict_item` VALUES ('10', '7', '12', '二恶烷若翁人', 'pig', '1', 'EQW', '1', '1.0.1', '管理员', '2020-07-02 22:42:15', '2020-07-02 22:42:15');
INSERT INTO `bpm_dict_item` VALUES ('11', '7', '1.0.1', 'test1', 'pig', '1', '', '1', '1.0.1', '管理员', '2020-08-03 17:31:04', '2020-08-03 17:31:05');
INSERT INTO `bpm_dict_item` VALUES ('12', '7', '1', 'test1', 'pig', '1', '', '1', '1.0.1', '管理员', '2020-08-03 17:31:10', '2020-08-03 17:31:10');
INSERT INTO `bpm_dict_item` VALUES ('13', '8', 'start', '发起', 'pig', '1', '发起', '1', '1.0.1', '管理员', '2020-08-03 17:40:03', '2020-08-03 17:40:04');
INSERT INTO `bpm_dict_item` VALUES ('14', '8', 'draft', '草稿', 'pig', '1', '草稿', '1', '1.0.1', '管理员', '2020-08-03 17:40:23', '2020-08-03 17:40:23');
INSERT INTO `bpm_dict_item` VALUES ('15', '8', 'pass', '审批通过', 'pig', '1', '审批通过', '1', '1.0.1', '管理员', '2020-08-03 17:41:10', '2020-08-03 17:41:10');
INSERT INTO `bpm_dict_item` VALUES ('16', '8', 'reject', '拒绝', 'pig', '1', '不同意', '1', '1.0.1', '管理员', '2020-08-03 17:41:49', '2020-08-03 17:41:49');
INSERT INTO `bpm_dict_item` VALUES ('17', '8', 'addSign', '加签', 'pig', '1', '会签', '1', '1.0.1', '管理员', '2020-08-03 18:14:15', '2020-08-03 18:14:16');
INSERT INTO `bpm_dict_item` VALUES ('18', '8', 'addTempNode', '新增临时节点', 'pig', '1', 'addTempNode', '1', '1.0.1', '管理员', '2020-08-03 17:42:35', '2020-08-03 17:42:36');
INSERT INTO `bpm_dict_item` VALUES ('19', '8', 'returnNode', '退回', 'pig', '1', '退回', '1', '1.0.1', '管理员', '2020-08-03 17:42:52', '2020-08-03 17:42:53');
INSERT INTO `bpm_dict_item` VALUES ('20', '8', 'returnRandomNode', '自由跳转', 'pig', '1', '自由跳转', '1', '1.0.1', '管理员', '2020-08-03 17:43:47', '2020-08-03 17:43:48');
INSERT INTO `bpm_dict_item` VALUES ('21', '8', 'counterSign', '会签', 'pig', '1', '', '1', '1.0.1', '管理员', '2020-08-03 18:14:00', '2020-08-03 18:14:01');

-- ----------------------------
-- Table structure for bpm_event
-- ----------------------------
DROP TABLE IF EXISTS `bpm_event`;
CREATE TABLE `bpm_event` (
  `event_id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '事件编号',
  `event_code` varchar(64) DEFAULT '' COMMENT '事件编码',
  `event_name` varchar(128) DEFAULT '' COMMENT '事件名称',
  `event_type` varchar(64) DEFAULT '' COMMENT '事件类型',
  `process_id` int(20) DEFAULT NULL COMMENT '流程编号',
  `node_id` varchar(255) DEFAULT '' COMMENT '触发节点编号列表',
  `request_url` varchar(255) DEFAULT '' COMMENT '请求地址',
  `request_method` varchar(64) DEFAULT '' COMMENT '请求方法',
  `request_headers` text COMMENT '请求头（json 格式）',
  `request_body` text COMMENT '请求体',
  `request_async` tinyint(2) DEFAULT '0' COMMENT '是否异步请求 0同步1异步 ',
  `response_save` tinyint(2) DEFAULT '1' COMMENT '1 保存返回结果至流程变量 0 不保存结果至流程变量',
  `response_var_name` varchar(64) DEFAULT '' COMMENT '接收返回类型变量名称',
  `order_no` int(10) DEFAULT '1' COMMENT '序号',
  `tenant_id` varchar(100) DEFAULT '' COMMENT '租户编号',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `valid_state` tinyint(2) DEFAULT '1' COMMENT '状态码 1有效 0 失效',
  `operator_id` int(20) DEFAULT NULL COMMENT '操作人工号',
  `operator_name` varchar(128) DEFAULT NULL COMMENT '操作人姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`event_id`),
  UNIQUE KEY `uniq_event_code` (`event_code`) USING BTREE,
  KEY `idx_process_id` (`process_id`),
  KEY `idx_node_id` (`node_id`),
  KEY `idx_event_type` (`event_type`),
  KEY `idx_tenant` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bpm_event
-- ----------------------------

-- ----------------------------
-- Table structure for bpm_file
-- ----------------------------
DROP TABLE IF EXISTS `bpm_file`;
CREATE TABLE `bpm_file` (
  `file_id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `file_instance_code` varchar(64) DEFAULT '' COMMENT '文件实例编号',
  `file_name` varchar(100) DEFAULT '' COMMENT '文件名称',
  `file_md5_name` varchar(255) DEFAULT '' COMMENT '加密后文件名称',
  `file_path` varchar(255) DEFAULT '' COMMENT '文件路径',
  `file_size` int(11) DEFAULT NULL COMMENT '文件大小',
  `file_extend` varchar(32) DEFAULT NULL COMMENT '文件后缀',
  `file_status` tinyint(2) unsigned DEFAULT '1' COMMENT '状态 1： 文件生成中 1.0.1：文件生成成功 3：文件生成失败',
  `file_system_code` varchar(50) DEFAULT '' COMMENT '所属系统',
  `file_paltform` varchar(50) DEFAULT '' COMMENT '所属平台',
  `file_service_name` varchar(100) DEFAULT '' COMMENT '所属服务名称',
  `file_method_name` varchar(100) DEFAULT '' COMMENT '所属于哪个方法',
  `file_owner_id` int(20) unsigned DEFAULT NULL COMMENT '文件所属人员',
  `file_owner_name` varchar(100) DEFAULT '' COMMENT '文件所属人姓名',
  `tenant_id` varchar(100) DEFAULT '' COMMENT '租户编号',
  `error_message` text COMMENT '错误日志',
  `valid_state` tinyint(2) DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` int(20) DEFAULT NULL COMMENT '操作人工号',
  `operator_name` varchar(100) DEFAULT '' COMMENT '操作人姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`file_id`),
  KEY `idx_system_code` (`file_system_code`),
  KEY `idx_file_name` (`file_name`),
  KEY `idx_instance_id` (`file_instance_code`),
  KEY `idx_tenant` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bpm_file
-- ----------------------------

-- ----------------------------
-- Table structure for bpm_file_templete
-- ----------------------------
DROP TABLE IF EXISTS `bpm_file_templete`;
CREATE TABLE `bpm_file_templete` (
  `tempalte_id` char(32) NOT NULL COMMENT 'GUID主键',
  `process_id` int(20) unsigned NOT NULL DEFAULT '0' COMMENT '流程编号',
  `tenant_id` varchar(64) NOT NULL DEFAULT '' COMMENT '租户编号',
  `file_name` varchar(255) DEFAULT '' COMMENT '模板文件名称',
  `file_md5_name` varchar(255) DEFAULT '' COMMENT '模板文件名称',
  `file_extend` varchar(255) DEFAULT '' COMMENT '文件后缀',
  `file_path` varchar(255) DEFAULT '' COMMENT '文件路径',
  `file_size` decimal(20,2) DEFAULT '0.00' COMMENT '文件大小',
  `remarks` varchar(255) DEFAULT '' COMMENT '备注',
  `sort` smallint(5) unsigned DEFAULT '1' COMMENT '排序值',
  `valid_state` tinyint(2) unsigned DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` int(20) unsigned DEFAULT '0' COMMENT '操作人工号',
  `operator_name` varchar(128) DEFAULT '' COMMENT '操作人姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后一次更新时间',
  PRIMARY KEY (`tempalte_id`),
  KEY `idx_process_id` (`process_id`),
  KEY `idx_tenant_id` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='模板文件表';

-- ----------------------------
-- Records of bpm_file_templete
-- ----------------------------

-- ----------------------------
-- Table structure for bpm_form
-- ----------------------------
DROP TABLE IF EXISTS `bpm_form`;
CREATE TABLE `bpm_form` (
  `form_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '表单编号',
  `form_key` varchar(64) NOT NULL DEFAULT '' COMMENT '表单KEY',
  `form_name` varchar(100) NOT NULL DEFAULT '' COMMENT '表单名称',
  `tenant_id` varchar(100) NOT NULL DEFAULT '' COMMENT '租户编号',
  `form_type` tinyint(3) NOT NULL DEFAULT '1' COMMENT '表单类型 1 PC 表单 1.0.1 移动表单',
  `sort` smallint(5) NOT NULL DEFAULT '1' COMMENT '排序',
  `form_data` text COMMENT '表单json数据',
  `remarks` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `valid_state` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `operator_id` bigint(20) DEFAULT '0' COMMENT '操作人工号',
  `operator_name` varchar(100) DEFAULT '' COMMENT '操作人姓名',
  PRIMARY KEY (`form_id`),
  UNIQUE KEY `uniq_form_key` (`form_key`),
  KEY `idx_form_key` (`form_key`),
  KEY `idx_tenant_id` (`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bpm_form
-- ----------------------------
INSERT INTO `bpm_form` VALUES ('1', 'pig:test2', '发的发斯蒂芬是', 'pig', '1', '1', null, '', '1', '2020-05-29 06:19:55', '2020-06-17 04:46:10', '1.0.1', '管理员');
INSERT INTO `bpm_form` VALUES ('1.0.1', 'pig:test3', '表单名称', 'pig', '1', '1', null, '', '1', '2020-05-29 07:15:54', '2020-06-16 10:21:46', '1.0.1', '管理员');
INSERT INTO `bpm_form` VALUES ('3', 'pig:test:111', '11eqwewq', 'pig', '1', '1', '{\"config\":{\"labelPosition\":\"left\",\"labelWidth\":15},\"list\":[{\"type\":\"text\",\"label\":\"文本域\",\"options\":{\"minWidth\":10,\"width\":100,\"height\":3,\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"hidden\":false,\"disabled\":false},\"model\":\"text_1592298938647\",\"key\":\"text_1592298938647\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"text\",\"label\":\"文本域\",\"options\":{\"minWidth\":10,\"width\":100,\"height\":3,\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"hidden\":false,\"disabled\":false},\"model\":\"text_1592281889678\",\"key\":\"text_1592281889678\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"checkbox\",\"label\":\"多选框\",\"options\":{\"disabled\":false,\"checkboxDefaultValue\":[],\"chooseMin\":0,\"chooseMax\":3,\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"选项1\"},{\"value\":\"1.0.1\",\"label\":\"选项2\"},{\"value\":\"3\",\"label\":\"选项3\"}]},\"model\":\"checkbox_1592298827271\",\"key\":\"checkbox_1592298827271\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"text\",\"label\":\"文本域\",\"options\":{\"minWidth\":10,\"width\":100,\"height\":3,\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"hidden\":false,\"disabled\":false},\"model\":\"text_1592282663974\",\"key\":\"text_1592282663974\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"select\",\"label\":\"下拉选择框\",\"options\":{\"width\":100,\"minWidth\":20,\"multiple\":false,\"defaultValue\":\"1\",\"multipleDefaultValue\":[],\"disabled\":false,\"clearable\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"选项1\"},{\"value\":\"1.0.1\",\"label\":\"选项2\"}],\"showSearch\":false},\"model\":\"select_1592281890669\",\"key\":\"select_1592281890669\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"checkbox\",\"label\":\"多选框\",\"options\":{\"disabled\":false,\"checkboxDefaultValue\":[],\"chooseMin\":0,\"chooseMax\":3,\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"选项1\"},{\"value\":\"1.0.1\",\"label\":\"选项2\"},{\"value\":\"3\",\"label\":\"选项3\"}]},\"model\":\"checkbox_1592281891541\",\"key\":\"checkbox_1592281891541\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"uploadImg\",\"label\":\"上传图片\",\"options\":{\"fileList\":[],\"multiple\":true,\"disabled\":false,\"width\":100,\"limit\":3,\"action\":\"http://cdn.kcz66.com/uploadFile.txt\",\"listType\":\"text\",\"headers\":{\"auuu\":\"auuu\"},\"data\":{\"wo\":123}},\"model\":\"uploadImg_1592282768516\",\"key\":\"uploadImg_1592282768516\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"childTable\",\"label\":\"子表\",\"list\":[],\"options\":{},\"model\":\"childTable_1592282761700\",\"key\":\"childTable_1592282761700\"}]}', '', '1', '2020-05-29 07:16:09', '2020-06-16 09:24:05', '1.0.1', '管理员');
INSERT INTO `bpm_form` VALUES ('4', 'pig:test:111s', '请求', 'pig', '1', '1', '{\"config\":{\"labelPosition\":\"left\",\"labelWidth\":15},\"list\":[{\"type\":\"select\",\"label\":\"下拉选择框\",\"options\":{\"width\":100,\"minWidth\":20,\"multiple\":false,\"defaultValue\":\"1\",\"multipleDefaultValue\":[],\"disabled\":false,\"clearable\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"选项1\"},{\"value\":\"1.0.1\",\"label\":\"选项2\"}],\"showSearch\":false},\"model\":\"select_1592282813275\",\"key\":\"select_1592282813275\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"checkbox\",\"label\":\"多选框\",\"options\":{\"disabled\":false,\"checkboxDefaultValue\":[],\"chooseMin\":0,\"chooseMax\":3,\"dynamicKey\":\"\",\"dynamic\":false,\"options\":[{\"value\":\"1\",\"label\":\"选项1\"},{\"value\":\"1.0.1\",\"label\":\"选项2\"},{\"value\":\"3\",\"label\":\"选项3\"}]},\"model\":\"checkbox_1592282813996\",\"key\":\"checkbox_1592282813996\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"uploadFile\",\"label\":\"上传文件\",\"options\":{\"fileList\":[],\"multiple\":true,\"disabled\":false,\"width\":100,\"limit\":3,\"buttonText\":\"点击上传\",\"warnText\":\"只能上传jpg/png文件，且不超过500kb\",\"action\":\"http://cdn.kcz66.com/uploadFile.txt\"},\"model\":\"uploadFile_1592282815484\",\"key\":\"uploadFile_1592282815484\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"button\",\"label\":\"按钮\",\"options\":{\"width\":0,\"maxWidth\":80,\"buttonType\":\"primary\",\"handle\":\"submit\",\"dynamicFun\":\"\",\"disabled\":false},\"key\":\"button_1592282816332\"},{\"type\":\"slider\",\"label\":\"滑动输入条\",\"options\":{\"width\":100,\"numberDefaultValue\":0,\"disabled\":false,\"min\":0,\"max\":100,\"step\":1,\"showInput\":true},\"model\":\"slider_1592282817132\",\"key\":\"slider_1592282817132\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"p\",\"label\":\"文字\",\"options\":{\"width\":100},\"key\":\"p_1592282817860\"}]}', 'terterterter的梵蒂冈电饭锅地方个地方官的法国大范甘迪法国大范甘迪购房个地方官的法国大范甘迪法国大范甘迪法国大范甘迪法国大范甘迪法国德国的法国德国的', '1', '2020-05-29 07:16:18', '2020-06-16 10:21:50', '1.0.1', '管理员');
INSERT INTO `bpm_form` VALUES ('12', 'pig:test5', '测试', 'pig', '1', '1', '{\"config\":{\"labelPosition\":\"left\",\"labelWidth\":15},\"list\":[{\"type\":\"divider\",\"label\":\"\",\"options\":{\"orientation\":\"left\"},\"key\":\"divider_1593339093459\"},{\"type\":\"card\",\"label\":\"表单信息\",\"list\":[{\"type\":\"input\",\"label\":\"输入框\",\"options\":{\"label\":\"输入框\",\"minWidth\":10,\"width\":100,\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"hidden\":false,\"disabled\":false},\"model\":\"input_1593338728959\",\"key\":\"input_1593338728959\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"select\",\"label\":\"下拉选择框\",\"options\":{\"width\":100,\"minWidth\":20,\"multiple\":false,\"defaultValue\":\"1\",\"multipleDefaultValue\":[],\"disabled\":false,\"clearable\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"dataKey\",\"dynamic\":true,\"options\":[{\"value\":\"1\",\"label\":\"选项1\"},{\"value\":\"1.0.1\",\"label\":\"选项2\"}],\"showSearch\":false},\"model\":\"select_1593338745618\",\"key\":\"select_1593338745618\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"input\",\"label\":\"输入框\",\"options\":{\"label\":\"输入框\",\"minWidth\":10,\"width\":100,\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"hidden\":false,\"disabled\":false},\"model\":\"input_1593338731601\",\"key\":\"input_1593338731601\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"input\",\"label\":\"输入框\",\"options\":{\"label\":\"输入框\",\"minWidth\":10,\"width\":100,\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"hidden\":false,\"disabled\":false},\"model\":\"input_1593338734651\",\"key\":\"input_1593338734651\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]}],\"key\":\"card_1593338665035\",\"options\":{}},{\"type\":\"card\",\"label\":\"审批记录\",\"list\":[{\"type\":\"table\",\"label\":\"表格布局\",\"trs\":[{\"tds\":[{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"输入框\",\"options\":{\"label\":\"输入框\",\"minWidth\":10,\"width\":100,\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"hidden\":false,\"disabled\":false},\"model\":\"input_1593339146118\",\"key\":\"input_1593339146118\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"输入框\",\"options\":{\"label\":\"输入框\",\"minWidth\":10,\"width\":100,\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"hidden\":false,\"disabled\":false},\"model\":\"input_1593339148935\",\"key\":\"input_1593339148935\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]}]}]},{\"tds\":[{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"输入框\",\"options\":{\"label\":\"输入框\",\"minWidth\":10,\"width\":100,\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"hidden\":false,\"disabled\":false},\"model\":\"input_1593339151652\",\"key\":\"input_1593339151652\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"input\",\"label\":\"输入框\",\"options\":{\"label\":\"输入框\",\"minWidth\":10,\"width\":100,\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"hidden\":false,\"disabled\":false},\"model\":\"input_1593339155553\",\"key\":\"input_1593339155553\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]}]}]}],\"options\":{\"bordered\":true,\"bright\":false,\"small\":true,\"customStyle\":\"\"},\"key\":\"table_1593339139418\"}],\"key\":\"card_1593339122426\",\"options\":{}},{\"type\":\"card\",\"label\":\"卡片布局\",\"list\":[{\"type\":\"table\",\"label\":\"表格布局\",\"trs\":[{\"tds\":[{\"colspan\":1,\"rowspan\":1.0.1,\"list\":[{\"type\":\"button\",\"label\":\"按钮\",\"options\":{\"width\":0,\"maxWidth\":80,\"buttonType\":\"primary\",\"handle\":\"doSomething\",\"dynamicFun\":\"\",\"disabled\":false},\"key\":\"button_1593339067318\"}]},{\"colspan\":1,\"rowspan\":1,\"list\":[{\"type\":\"button\",\"label\":\"按钮\",\"options\":{\"width\":0,\"maxWidth\":80,\"buttonType\":\"primary\",\"handle\":\"submit\",\"dynamicFun\":\"\",\"disabled\":false},\"key\":\"button_1593339083285\"}]}]},{\"tds\":[{\"colspan\":1,\"rowspan\":1,\"list\":[]}]}],\"options\":{\"bordered\":true,\"bright\":false,\"small\":true,\"customStyle\":\"\"},\"key\":\"table_1593339190718\"}],\"key\":\"card_1593339175460\",\"options\":{}}],\"dynamicKeyList\":[]}', '测试', '1', '2020-06-15 07:35:37', '2020-06-28 18:13:37', '1.0.1', '管理员');
INSERT INTO `bpm_form` VALUES ('15', 'pig:test111111', '请求', 'pig', '1', '1', '{\"config\":{\"labelPosition\":\"left\",\"labelWidth\":15},\"list\":[{\"type\":\"select\",\"label\":\"下拉选择框\",\"options\":{\"width\":100,\"minWidth\":20,\"multiple\":false,\"defaultValue\":\"1\",\"multipleDefaultValue\":[],\"disabled\":false,\"clearable\":false,\"placeholder\":\"请选择\",\"dynamicKey\":\"pig:dict:YUTUTYU\",\"dynamic\":true,\"options\":[{\"value\":\"1\",\"label\":\"选项1\"},{\"value\":\"1.0.1\",\"label\":\"选项2\"}],\"showSearch\":false},\"model\":\"select_1592282813275\",\"key\":\"select_1592282813275\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"checkbox\",\"label\":\"多选框\",\"options\":{\"disabled\":false,\"checkboxDefaultValue\":[],\"chooseMin\":0,\"chooseMax\":3,\"dynamicKey\":\"pig:dict:validState\",\"dynamic\":true,\"options\":[{\"value\":\"1\",\"label\":\"选项1\"},{\"value\":\"1.0.1\",\"label\":\"选项2\"},{\"value\":\"3\",\"label\":\"选项3\"}]},\"model\":\"checkbox_1592282813996\",\"key\":\"checkbox_1592282813996\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"uploadFile\",\"label\":\"上传文件\",\"options\":{\"fileList\":[],\"multiple\":true,\"disabled\":false,\"width\":100,\"limit\":3,\"buttonText\":\"点击上传\",\"warnText\":\"只能上传jpg/png文件，且不超过500kb\",\"action\":\"http://cdn.kcz66.com/uploadFile.txt\"},\"model\":\"uploadFile_1592282815484\",\"key\":\"uploadFile_1592282815484\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"button\",\"label\":\"按钮\",\"options\":{\"width\":0,\"maxWidth\":80,\"buttonType\":\"primary\",\"handle\":\"submit\",\"dynamicFun\":\"\",\"disabled\":false},\"key\":\"button_1592282816332\"},{\"type\":\"slider\",\"label\":\"滑动输入条\",\"options\":{\"width\":100,\"numberDefaultValue\":0,\"disabled\":false,\"min\":0,\"max\":100,\"step\":1,\"showInput\":true},\"model\":\"slider_1592282817132\",\"key\":\"slider_1592282817132\",\"rules\":[{\"required\":false,\"message\":\"必填项\",\"trigger\":\"blur\"}]},{\"type\":\"p\",\"label\":\"文字\",\"options\":{\"width\":100},\"key\":\"p_1592282817860\"}],\"dynamicKeyList\":[\"pig:dict:YUTUTYU\",\"pig:dict:validState\"]}', 'terterterter的梵蒂冈电饭锅地方个地方官的法国大范甘迪法国大范甘迪购房个地方官的法国大范甘迪法国大范甘迪法国大范甘迪法国大范甘迪法国德国的法国德国的', '1', '2020-06-17 04:45:07', '2020-08-03 17:28:43', '1.0.1', '管理员');

-- ----------------------------
-- Table structure for bpm_form_data
-- ----------------------------
DROP TABLE IF EXISTS `bpm_form_data`;
CREATE TABLE `bpm_form_data` (
  `data_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '数据ID',
  `data_key` varchar(64) NOT NULL DEFAULT '' COMMENT '数据KEY',
  `data_name` varchar(255) NOT NULL DEFAULT '' COMMENT '数据中文名称',
  `string_value` varchar(4000) DEFAULT '' COMMENT '字符串数据值',
  `boolean_value` tinyint(1) DEFAULT NULL COMMENT 'boolean 值',
  `number_value` decimal(50,4) DEFAULT NULL COMMENT '数值值',
  `number_format` varchar(50) DEFAULT '' COMMENT '日期格式',
  `date_value` datetime DEFAULT NULL COMMENT '日期格式',
  `date_pattern` varchar(64) DEFAULT 'yyyy-MM-dd' COMMENT '日期格式',
  `select_value` varchar(50) DEFAULT '' COMMENT 'select 选中值',
  `select_item` varchar(2000) DEFAULT '' COMMENT 'SELECT 下拉选项',
  `text_value` longtext COMMENT '大字段',
  `data_type` varchar(64) DEFAULT '' COMMENT '数据类型',
  `form_id` bigint(20) DEFAULT NULL COMMENT '表单编号',
  `form_key` varchar(255) DEFAULT NULL COMMENT '表单KEY',
  `tenant_id` varchar(100) DEFAULT '' COMMENT '租户',
  `process_id` bigint(20) DEFAULT NULL COMMENT '流程编号',
  `apply_id` bigint(20) DEFAULT NULL COMMENT '申请编号',
  `proc_inst_id` varchar(64) DEFAULT NULL COMMENT '流程实例编号',
  `task_id` bigint(20) DEFAULT NULL COMMENT '任务编号',
  `valid_state` tinyint(2) unsigned DEFAULT '1' COMMENT '有效状态；0表示无效，1表示有效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'create_time',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `operator_id` bigint(20) unsigned DEFAULT '0' COMMENT '操作人工号',
  `operator_name` varchar(64) DEFAULT '' COMMENT 'operator_name',
  PRIMARY KEY (`data_id`),
  KEY `idx_apply_id` (`apply_id`),
  KEY `idx_tenant_id` (`tenant_id`),
  KEY `idx_proc_inst_id` (`proc_inst_id`),
  KEY `idx_process_id` (`process_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for bpm_history
-- ----------------------------
DROP TABLE IF EXISTS `bpm_history`;
CREATE TABLE `bpm_history` (
  `history_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '审批记录表',
  `apply_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '申请编号',
  `task_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '任务编号',
  `task_name` varchar(255) NOT NULL DEFAULT '' COMMENT '任务名称',
  `tenant_id` varchar(128) NOT NULL DEFAULT '' COMMENT '租户编号',
  `system` varchar(64) DEFAULT '' COMMENT '来源系统',
  `paltform` varchar(64) DEFAULT '' COMMENT '来源平台',
  `approve_action_code` varchar(64) DEFAULT '' COMMENT '审批动作编码',
  `approve_action_name` varchar(255) DEFAULT '' COMMENT '审批动作名称',
  `approve_opinion` varchar(4000) DEFAULT '' COMMENT '审批意见',
  `approve_user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '审批人工号',
  `approve_real_name` varchar(128) NOT NULL DEFAULT '' COMMENT '审批人姓名',
  `remarks` varchar(255) DEFAULT '' COMMENT '备注',
  `valid_state` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '操作人工号',
  `operator_name` varchar(255) NOT NULL DEFAULT '' COMMENT '操作人工姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`history_id`),
  KEY `idx_apply_id` (`apply_id`),
  KEY `idx_task_id` (`task_id`),
  KEY `idx_tenant` (`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='审批历史表';


-- ----------------------------
-- Table structure for bpm_menu
-- ----------------------------
DROP TABLE IF EXISTS `bpm_menu`;
CREATE TABLE `bpm_menu` (
  `menu_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `menu_code` varchar(128) NOT NULL DEFAULT '' COMMENT '资源编码',
  `menu_name` varchar(128) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `menu_icon` varchar(255) NOT NULL DEFAULT '' COMMENT '菜单编码',
  `menu_url` varchar(255) NOT NULL DEFAULT '' COMMENT '菜单URL',
  `menu_type` varchar(32) NOT NULL DEFAULT 'menu' COMMENT '菜单类型 menu: 菜单',
  `meta` varchar(255) NOT NULL DEFAULT '' COMMENT '元数据',
  `always_show` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否总是显示 1 是 0 否',
  `parent_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '上级编号 0为 1级',
  `tenant_id` varchar(128) NOT NULL DEFAULT '' COMMENT '租户编号',
  `component` varchar(255) NOT NULL DEFAULT '' COMMENT '组件地址',
  `hidden` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否隐藏 0 不隐藏 1 隐藏',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT '排序',
  `redirect` varchar(255) NOT NULL DEFAULT '' COMMENT '重定向值',
  `remarks` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `valid_state` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '状态 0 失效 1 有效',
  `operator_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '操作者工号',
  `operator_name` varchar(128) NOT NULL DEFAULT '' COMMENT '操作人名称',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `uniq_code` (`menu_code`),
  KEY `idx_tenant` (`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COMMENT='菜单表';

-- ----------------------------
-- Records of bpm_menu
-- ----------------------------
INSERT INTO `bpm_menu` VALUES ('1', 'pig:menu:dashboard', '首页', 'dashboard', '/dashboard', 'menu', '', '0', '0', 'pig', 'Layout', '0', '1', '', '首页', '1', '0', '', '2020-07-09 16:23:57', '2020-07-11 08:47:17');
INSERT INTO `bpm_menu` VALUES ('1.0.1', 'pig:menu:usertask', '用户任务', 'example', '/usertask', 'menu', '', '0', '0', 'pig', 'Layout', '0', '1.0.1', 'todo', '', '1', '0', '', '2020-07-09 16:23:57', '2020-07-12 14:44:01');
INSERT INTO `bpm_menu` VALUES ('3', 'pig:menu:todo', '我的待办', 'tree', '/todo', 'menu', '', '0', '1.0.1', 'pig', 'usertask/todo', '0', '1', '', '', '1', '0', '', '2020-07-09 16:23:57', '2020-07-11 15:53:05');
INSERT INTO `bpm_menu` VALUES ('4', 'pig:menu:apply', '我的申请', 'table', '/apply', 'menu', '', '0', '1.0.1', 'pig', 'usertask/apply', '0', '1.0.1', '', '', '1', '0', '', '2020-07-09 16:23:57', '2020-07-11 08:47:21');
INSERT INTO `bpm_menu` VALUES ('5', 'pig:menu:draft', '我的草稿', 'tree', '/draft', 'menu', '', '0', '1.0.1', 'pig', 'usertask/draft', '0', '3', '', '', '1', '0', '', '2020-07-09 16:23:57', '2020-07-11 08:47:22');
INSERT INTO `bpm_menu` VALUES ('6', 'pig:menu:haveDo', '我的已办', 'table', '/haveDo', 'menu', '', '0', '1.0.1', 'pig', 'usertask/haveDo', '0', '4', '', '', '1', '0', '', '2020-07-09 16:30:18', '2020-07-11 08:47:23');
INSERT INTO `bpm_menu` VALUES ('7', 'pig:menu:dict', '字典列表', 'form', '/dict', 'menu', '', '0', '13', 'pig', 'dict/dictList', '0', '1', '', '', '1', '0', '', '2020-07-09 16:23:57', '2020-07-11 08:47:23');
INSERT INTO `bpm_menu` VALUES ('8', 'pig:menu:formDesign', '表单设计器', 'tree', '/form', 'menu', '', '0', '0', 'pig', 'Layout', '0', '3', 'formList', '', '1', '0', '', '2020-07-09 16:23:57', '2020-08-11 14:35:45');
INSERT INTO `bpm_menu` VALUES ('9', 'pig:menu:process1', '流程设计器', 'tree', '/process', 'menu', '', '0', '0', 'pig', 'Layout', '0', '4', 'processList', '', '1', '0', '', '2020-07-09 16:23:57', '2020-08-12 23:28:23');
INSERT INTO `bpm_menu` VALUES ('10', 'pig:menu:api', 'API接口文档', 'link', 'http://120.77.218.141:9993/api/bpm/swagger-ui.html', 'menu', '', '0', '14', 'pig', 'iframe', '0', '9999', '', '', '1', '0', '', '2020-07-09 16:38:42', '2020-08-20 09:09:42');
INSERT INTO `bpm_menu` VALUES ('11', 'pig:menu:formList', '表单列表', 'tree', '/formList', 'menu', '', '0', '8', 'pig', 'form/formList', '0', '3', '', '', '1', '0', '', '2020-07-09 16:23:57', '2020-07-11 08:47:27');
INSERT INTO `bpm_menu` VALUES ('12', 'pig:menu:processList', '流程列表', 'tree', '/processList', 'menu', '', '0', '9', 'pig', 'process/processList', '0', '1', '', '', '1', '0', '', '2020-07-09 16:23:57', '2020-08-11 14:35:02');
INSERT INTO `bpm_menu` VALUES ('13', 'pig:menu:setting', '系统管理', 'tree', '/setting', 'menu', '', '0', '0', 'pig', 'Layout', '0', '5', 'dictList', '', '1', '0', '', '2020-07-09 16:23:57', '2020-07-12 14:44:45');
INSERT INTO `bpm_menu` VALUES ('14', 'pig:menu:iframe', 'API', 'el-icon-document', '/iframe', 'menu', '', '0', '0', 'pig', 'Layout', '0', '5', '', '', '1', '0', '', '2020-07-09 16:23:57', '2020-07-11 12:48:11');
INSERT INTO `bpm_menu` VALUES ('15', 'pig:menu:api:baidu', '百度', 'link', 'http://www.baidu.com', 'menu', '', '0', '14', 'pig', 'iframe', '0', '9999', '', '', '1', '0', '', '2020-07-09 16:38:42', '2020-09-02 09:21:45');
INSERT INTO `bpm_menu` VALUES ('16', 'pig:menu:formBuild', '我的流程', 'example', '/formBuild', 'menu', '', '0', '0', 'pig', 'Layout', '0', '1.0.1', '', '', '1', '0', '', '2020-07-09 16:23:57', '2020-07-11 15:48:16');
INSERT INTO `bpm_menu` VALUES ('17', 'pig:menu:startProcess', '测试流程1', 'example', 'start', 'menu', '{\"processKey\":\"pig_test1\"}', '0', '16', 'pig', 'formBuild/start', '0', '1', '', '', '1', '0', '', '2020-07-09 16:23:57', '2020-07-12 14:36:57');
INSERT INTO `bpm_menu` VALUES ('18', 'pig:menu:api:nacos', 'nacos', 'link', 'http://120.77.218.141:8848/nacos/#/', 'menu', '', '0', '14', 'pig', 'iframe', '0', '9999', '', '', '1', '0', '', '2020-07-09 16:38:42', '2020-08-12 23:24:19');
INSERT INTO `bpm_menu` VALUES ('19', 'pig:menu:api:jenkins', 'jenkins', 'link', 'http://120.77.218.141:8083/', 'menu', '', '0', '14', 'pig', 'iframe', '0', '9999', '', '', '1', '0', '', '2020-07-09 16:38:42', '2020-08-12 23:24:14');
INSERT INTO `bpm_menu` VALUES ('20', 'pig:menu:api:maven', 'maven', 'link', 'http://120.77.218.141:8086/repository/maven-snapshots/', 'menu', '', '0', '14', 'pig', 'iframe', '0', '9999', '', '', '1', '0', '', '2020-07-09 16:38:42', '2020-08-12 23:24:08');
INSERT INTO `bpm_menu` VALUES ('21', 'pig:menu:api:sql', 'SQL平台', 'link', 'http://120.77.218.141:9995', 'menu', '', '0', '14', 'pig', '', '0', '9999', '', '', '1', '0', '', '2020-07-09 16:38:42', '2020-08-28 18:04:03');

-- ----------------------------
-- Table structure for bpm_node
-- ----------------------------
DROP TABLE IF EXISTS `bpm_node`;
CREATE TABLE `bpm_node` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `node_id` varchar(64) NOT NULL DEFAULT '' COMMENT '节点编号',
  `node_name` varchar(64) NOT NULL DEFAULT '' COMMENT '节点名称',
  `definition_id` varchar(64) NOT NULL DEFAULT '' COMMENT '版本号',
  `process_id` bigint(20) DEFAULT NULL COMMENT '流程编号',
  `process_key` varchar(64) NOT NULL DEFAULT '' COMMENT '流程key',
  `tenant_id` varchar(100) NOT NULL DEFAULT '' COMMENT '租户编号',
  `node_type` varchar(64) NOT NULL DEFAULT '' COMMENT '节点类型',
  `task_type` varchar(64) DEFAULT 'approve' COMMENT '任务类型 当前任务类型  start 起草 approve 审批  record  备案 archive 归档',
  `priority` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT '优先级',
  `form_key` varchar(64) NOT NULL DEFAULT '' COMMENT '关联表单KEY',
  `form_name` varchar(255) NOT NULL DEFAULT '' COMMENT '表单名称',
  `user_id_list` varchar(255) NOT NULL DEFAULT '' COMMENT '节点人员编号列表',
  `user_name_list` varchar(255) NOT NULL DEFAULT '' COMMENT '节点人员名称列表，。多个人以 , 区分',
  `role_group_code` varchar(64) NOT NULL DEFAULT '' COMMENT '节点角色组编号',
  `role_group_name` varchar(255) NOT NULL DEFAULT '' COMMENT '角色组名称',
  `role_code` varchar(64) NOT NULL DEFAULT '' COMMENT '角色编码',
  `role_name` varchar(255) NOT NULL DEFAULT '' COMMENT '角色名称',
  `find_user_type` tinyint(4) NOT NULL DEFAULT '4' COMMENT '节点用户类型 组合方式：1  角色组 1.0.1. 角色 3. 固定人员 4. 前端指定人员 5:申请人 6:同节点人员',
  `combine_type` tinyint(2) unsigned DEFAULT '1.0.1' COMMENT '组合方式：1 正常(找不到节点人员提示异常) 1.0.1 正常（找不到节点人员就跳过当前环节） ',
  `assignee_field` varchar(64) DEFAULT '' COMMENT '用户节点人员分配字段名称',
  `select_path` tinyint(2) DEFAULT '0' COMMENT '是否选择路径 0 否 1 是',
  `handler_strategy` varchar(64) DEFAULT 'skip' COMMENT '节点处理策略 skip: 执行人为空跳过,admin: 为空时管理员处理,error:为空时报错',
  `relation_node_id` varchar(64) DEFAULT '' COMMENT '依赖节点',
  `action_list` varchar(255) DEFAULT 'pass,reject' COMMENT '动作集合',
  `skip_expression` varchar(1000) DEFAULT '' COMMENT '用户任务条件跳过表达式',
  `expression` varchar(1000) DEFAULT '' COMMENT '连线表达式',
  `source_ref` varchar(64) DEFAULT '' COMMENT '连线来源节点NodeId',
  `target_ref` varchar(64) DEFAULT '' COMMENT '连线目标节点NodeId',
  `sequential` varchar(32) DEFAULT 'parallel' COMMENT '用户任务 多实例属性 parallel 并行审批，sequential 串行审批',
  `proportion` varchar(4) DEFAULT '' COMMENT '通过比例',
  `remarks` varchar(255) DEFAULT '' COMMENT '备注',
  `valid_state` tinyint(2) unsigned DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` bigint(20) DEFAULT NULL COMMENT '操作人工号',
  `operator_name` varchar(64) DEFAULT '' COMMENT '操作人姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_nid_pid` (`node_id`,`process_id`,`definition_id`) USING BTREE,
  KEY `idx_process_id` (`process_id`),
  KEY `idx_process_key` (`process_key`),
  KEY `idx_pid_nid` (`node_id`,`process_id`),
  KEY `idx_tenant` (`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='流程节点表';

-- ----------------------------
-- Table structure for bpm_node_user
-- ----------------------------
DROP TABLE IF EXISTS `bpm_node_user`;
CREATE TABLE `bpm_node_user` (
  `node_user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '节点人员名称',
  `apply_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '申请编号',
  `proc_inst_id` varchar(64) NOT NULL DEFAULT '' COMMENT '流程实例编号',
  `process_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '流程编号',
  `process_key` varchar(64) NOT NULL DEFAULT '' COMMENT '流程KEY',
  `node_id` varchar(64) NOT NULL DEFAULT '' COMMENT '节点编号',
  `node_name` varchar(64) NOT NULL DEFAULT '' COMMENT '节点名称',
  `parent_node_id` varchar(64) DEFAULT '' COMMENT '父节点编号',
  `parent_node_name` varchar(64) DEFAULT '' COMMENT '父节点名称',
  `definition_id` varchar(64) NOT NULL DEFAULT '' COMMENT '流程定义编号',
  `tenant_id` varchar(100) NOT NULL DEFAULT '' COMMENT '租户编号',
  `assignee_user_id_list` varchar(255) NOT NULL DEFAULT '' COMMENT '节点分配人员工号 多个人以 , 区分',
  `assignee_user_name_list` varchar(255) NOT NULL DEFAULT '' COMMENT '节点分配人员姓名 多个人以 , 区分',
  `skip` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '当处理策略为 节点找不到人时 跳过则为 1，否则为 0',
  `defaultSetAdmin` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '当处理策略为 节点找不到人时 管理员处理则为 1，否则为 0',
  `error` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '当处理策略为 节点找不到人时 抛出异常处理则为 1，否则为 0',
  `remarks` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `valid_state` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '操作人工号',
  `operator_name` varchar(255) NOT NULL DEFAULT '' COMMENT '操作人姓名',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`node_user_id`),
  KEY `idx_apply_id` (`apply_id`),
  KEY `idx_proc_inst_id` (`proc_inst_id`),
  KEY `idx_definition_id` (`definition_id`),
  KEY `idx_tenant_id` (`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='节点人员表';


-- ----------------------------
-- Table structure for bpm_process
-- ----------------------------
DROP TABLE IF EXISTS `bpm_process`;
CREATE TABLE `bpm_process` (
  `process_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '流程编码',
  `process_key` varchar(64) NOT NULL DEFAULT '' COMMENT '流程key',
  `process_name` varchar(255) NOT NULL DEFAULT '' COMMENT '流程名称',
  `process_menu_id` bigint(20) unsigned DEFAULT NULL COMMENT '流程归属菜单编号',
  `process_abbr` varchar(64) DEFAULT '' COMMENT '流程简称',
  `process_type` tinyint(2) unsigned DEFAULT '1' COMMENT '流程分类 1 正常',
  `company_id` bigint(20) DEFAULT NULL COMMENT '所属公司',
  `company_code` varchar(64) DEFAULT '' COMMENT '公司编码',
  `sort` smallint(6) unsigned DEFAULT '1' COMMENT '排序',
  `tenant_id` varchar(128) DEFAULT '' COMMENT '租户编号',
  `process_detail_id` bigint(20) unsigned DEFAULT '0' COMMENT '默认流程详细编号',
  `process_status` tinyint(3) unsigned DEFAULT '1' COMMENT '流程状态: 1 未发布 1.0.1 已发布',
  `remarks` varchar(255) DEFAULT '' COMMENT '流程备注',
  `valid_state` tinyint(2) DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` bigint(20) DEFAULT NULL COMMENT '操作人工号',
  `operator_name` varchar(255) DEFAULT '' COMMENT '操作人姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`process_id`),
  UNIQUE KEY `uniq_process_key` (`process_key`),
  KEY `index_process_key` (`process_key`),
  KEY `idx_tenant` (`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='流程表';


-- ----------------------------
-- Table structure for bpm_process_detail
-- ----------------------------
DROP TABLE IF EXISTS `bpm_process_detail`;
CREATE TABLE `bpm_process_detail` (
  `process_detail_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '流程详细编号',
  `tenant_id` varchar(128) NOT NULL DEFAULT '' COMMENT '租户编号',
  `process_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `process_xml` mediumtext COMMENT '流程XML格式数据',
  `definition_id` varchar(128) NOT NULL DEFAULT '' COMMENT '默认版本号',
  `apply_title_rule` varchar(255) DEFAULT '' COMMENT '申请标题规则',
  `apply_due_date` datetime DEFAULT NULL COMMENT '流程到期时间',
  `auto_complete_first_node` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '是否自动完成第一个节点任务 1 是 0 否',
  `publish_status` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '1 未发布 1.0.1 已发布 ',
  `main_version` tinyint(2) unsigned NOT NULL DEFAULT '1.0.1' COMMENT '1 默认版本 1.0.1 非默认版本',
  `remarks` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `valid_state` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `operator_name` varchar(255) NOT NULL DEFAULT '' COMMENT '操作人姓名',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`process_detail_id`),
  KEY `idx_process_id` (`process_id`),
  KEY `idx_def_id` (`definition_id`),
  KEY `idx_tenant_id` (`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4;


-- ----------------------------
-- Table structure for bpm_process_rule
-- ----------------------------
DROP TABLE IF EXISTS `bpm_process_rule`;
CREATE TABLE `bpm_process_rule` (
  `rule_id` char(32) NOT NULL DEFAULT '' COMMENT '规则 guid',
  `rule_name` varchar(255) NOT NULL DEFAULT '' COMMENT '规则名称',
  `rule_code` varchar(255) NOT NULL DEFAULT '' COMMENT '规则编号',
  `rule_type` smallint(5) NOT NULL DEFAULT '1' COMMENT '规则类型 1 人员规则',
  `tenant_id` varchar(64) NOT NULL,
  `process_id` int(20) unsigned NOT NULL DEFAULT '0' COMMENT '流程编号',
  `role_group_id` int(20) unsigned NOT NULL DEFAULT '0' COMMENT '角色组编号',
  `role_id` int(20) unsigned NOT NULL DEFAULT '0' COMMENT '角色编号',
  `user_id` int(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户编号',
  `rule_expression` varchar(255) NOT NULL DEFAULT '' COMMENT '规则表达式',
  `rule_action` varchar(255) NOT NULL DEFAULT '' COMMENT '规则动作',
  `rule_users` varchar(255) NOT NULL DEFAULT '' COMMENT '规则涉及人员（新增/减少）多个人以‘,’区分',
  `remarks` varchar(255) NOT NULL,
  `valid_state` tinyint(2) NOT NULL,
  `operator_id` int(20) NOT NULL,
  `operator_name` varchar(128) NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`rule_id`),
  UNIQUE KEY `uniq_rule_code` (`rule_code`),
  KEY `idx_tenant` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bpm_process_rule
-- ----------------------------

-- ----------------------------
-- Table structure for bpm_role
-- ----------------------------
DROP TABLE IF EXISTS `bpm_role`;
CREATE TABLE `bpm_role` (
  `role_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色编号',
  `role_code` varchar(64) NOT NULL DEFAULT '' COMMENT '角色编码',
  `role_name` varchar(255) NOT NULL DEFAULT '' COMMENT '角色名称',
  `role_level` tinyint(3) NOT NULL DEFAULT '1' COMMENT '角色级别',
  `role_abbr` varchar(64) DEFAULT '' COMMENT '角色简称',
  `role_alias_name` varchar(255) DEFAULT '' COMMENT '角色别名',
  `tenant_id` varchar(128) DEFAULT '' COMMENT '租户编号',
  `company_id` bigint(20) unsigned DEFAULT '0' COMMENT '归属公司编号',
  `dept_id` bigint(20) unsigned DEFAULT '0' COMMENT '归属部门编号',
  `remarks` varchar(255) DEFAULT '' COMMENT '备注',
  `valid_state` tinyint(3) unsigned DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` bigint(20) unsigned DEFAULT NULL COMMENT '操作人工号',
  `operator_name` varchar(255) DEFAULT '' COMMENT '操作人姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `uniq_code` (`role_code`),
  KEY `idx_tenant` (`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COMMENT='角色表';

-- ----------------------------
-- Records of bpm_role
-- ----------------------------
INSERT INTO `bpm_role` VALUES ('1', 'pig:administrastor', '【测试集团】管理员', '1', '管理员', '管理员', 'pig', '1', '1', '', '1', null, '', '2020-06-14 06:45:49', '2020-06-14 06:47:03');
INSERT INTO `bpm_role` VALUES ('1.0.1', 'pig:role:techDirector', '总公司技术部负责人', '1', '总公司技术部负责人', '总公司技术部负责人', 'pig', '1', '1', '', '1', null, '', '2020-06-24 13:52:47', '2020-06-24 14:05:37');
INSERT INTO `bpm_role` VALUES ('3', 'pig:role:techManager', '总公司技术部经理', '1', '总公司技术部经理', '总公司技术部经理', 'pig', '1', '1', '', '1', null, '', '2020-06-24 14:04:28', '2020-06-24 14:06:26');
INSERT INTO `bpm_role` VALUES ('4', 'pig:role:techStaff', '总公司技术部职员', '1', '总公司技术部职员', '总公司技术部职员', 'pig', '1', '1', '', '1', null, '', '2020-06-24 14:06:23', '2020-06-24 14:07:47');
INSERT INTO `bpm_role` VALUES ('5', 'pig:role:hairman', '董事长', '1', '董事长', '董事长', 'pig', '1', '1.0.1', '', '1', null, '', '2020-06-24 14:07:31', '2020-06-24 14:08:03');
INSERT INTO `bpm_role` VALUES ('6', 'pig:role:generalManager', '总经理', '1', '总经理', '总经理', 'pig', '1', '1.0.1', '', '1', null, '', '2020-06-24 14:08:41', '2020-06-24 14:08:41');
INSERT INTO `bpm_role` VALUES ('7', 'pig:role:financeDirector', '总公司财务部负责人', '1', '总公司财务部负责人', '总公司财务部负责人', 'pig', '1', '3', '', '1', null, '', '2020-06-24 14:09:00', '2020-06-24 14:09:25');
INSERT INTO `bpm_role` VALUES ('8', 'pig:role:financeManager', '总公司财务部经理', '1', '总公司财务部经理', '总公司财务部经理', 'pig', '1', '3', '', '1', null, '', '2020-06-24 14:09:00', '2020-06-24 14:09:25');
INSERT INTO `bpm_role` VALUES ('9', 'pig:role:financeStaff', '总公司财务部职员', '1', '总公司财务部职员', '总公司财务部职员', 'pig', '1', '3', '', '1', null, '', '2020-06-24 14:09:00', '2020-06-24 14:09:25');
INSERT INTO `bpm_role` VALUES ('10', 'pig:role:gdBranchCompany:generalManager', '广分总经理', '1', '广分总经理', '广分总经理', 'pig', '4', '5', '', '1', null, '', '2020-06-24 14:12:15', '2020-06-24 14:12:24');
INSERT INTO `bpm_role` VALUES ('11', 'pig:role:gdBranchCompany:techDirector', '广分技术部负责人', '1', '广分技术部负责人', '广分技术部负责人', 'pig', '4', '4', '', '1', null, '', '2020-06-24 14:12:45', '2020-06-24 14:13:17');
INSERT INTO `bpm_role` VALUES ('12', 'pig:role:gdBranchCompany:techStaff', '广分技术部职员', '1', '广分技术部职员', '广分技术部职员', 'pig', '4', '4', '', '1', null, '', '2020-06-24 14:13:50', '2020-06-24 14:15:41');
INSERT INTO `bpm_role` VALUES ('13', 'pig:role:gdBranchCompany:financeDirector', '广分财务部负责人', '1', '广分财务部负责人', '广分财务部负责人', 'pig', '4', '6', '', '1', null, '', '2020-06-24 14:13:50', '2020-06-24 14:15:46');
INSERT INTO `bpm_role` VALUES ('14', 'pig:role:gdBranchCompany:financeStaff', '广分财务部职员', '1', '广分财务部职员', '广分财务部职员', 'pig', '4', '6', '', '1', null, '', '2020-06-24 14:13:50', '2020-06-24 14:15:48');

-- ----------------------------
-- Table structure for bpm_role_group
-- ----------------------------
DROP TABLE IF EXISTS `bpm_role_group`;
CREATE TABLE `bpm_role_group` (
  `role_group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_group_code` varchar(64) NOT NULL DEFAULT '' COMMENT '角色组编码',
  `role_group_name` varchar(255) NOT NULL DEFAULT '' COMMENT '角色组名称',
  `role_group_abbr` varchar(64) NOT NULL DEFAULT '' COMMENT '角色组简称',
  `business_line` varchar(64) NOT NULL DEFAULT '' COMMENT '所属条线，如 level 1.0.1 会根据 条线 找 level 1',
  `role_group_level` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '角色组等级',
  `role_group_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '角色组类别',
  `tenant_id` varchar(128) NOT NULL DEFAULT '' COMMENT '租户编号',
  `remarks` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `valid_state` tinyint(3) unsigned DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` bigint(20) unsigned DEFAULT NULL COMMENT '操作人工号',
  `operator_name` varchar(255) DEFAULT '' COMMENT '操作人姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`role_group_id`),
  UNIQUE KEY `uniq_code` (`role_group_code`),
  KEY `idx_tenant` (`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COMMENT='角色组';

-- ----------------------------
-- Records of bpm_role_group
-- ----------------------------
INSERT INTO `bpm_role_group` VALUES ('1', 'pig:roleGroup:testCompany:techDirector', '总公司技术部负责人', 'CTO', 'tech', '1', '1', 'pig', '', '1', null, '', '2020-06-24 11:08:48', '2020-06-24 13:51:50');
INSERT INTO `bpm_role_group` VALUES ('1.0.1', 'pig:roleGroup:testCompany:techManager', '总公司技术部经理', '', 'tech', '1', '1', 'pig', '', '1', null, '', '2020-06-24 11:12:43', '2020-06-24 13:51:53');
INSERT INTO `bpm_role_group` VALUES ('3', 'pig:roleGroup:testCompany:techStaff', '总公司技术部职员', '', 'tech', '1', '1', 'pig', '', '1', null, '', '2020-06-24 11:12:43', '2020-06-24 13:51:55');
INSERT INTO `bpm_role_group` VALUES ('4', 'pig:roleGroup:testCompany:hairman', '董事长', 'CEO', 'manager', '1', '1', 'pig', '', '1', null, '', '2020-06-24 11:22:07', '2020-06-24 13:51:57');
INSERT INTO `bpm_role_group` VALUES ('5', 'pig:roleGroup:testCompany:generalManager', '总经理', '', 'manager', '1', '1', 'pig', '', '1', null, '', '2020-06-24 11:23:22', '2020-06-24 13:52:00');
INSERT INTO `bpm_role_group` VALUES ('6', 'pig:roleGroup:testCompany:financeDirector', '总公司财务部负责人', 'CFO', 'finance', '1', '1', 'pig', '', '1', null, '', '2020-06-24 11:25:21', '2020-06-24 13:52:02');
INSERT INTO `bpm_role_group` VALUES ('7', 'pig:roleGroup:testCompany:financeManager', '总公司财务部经理', '', 'finance', '1', '1', 'pig', '', '1', null, '', '2020-06-24 11:26:10', '2020-06-24 13:52:05');
INSERT INTO `bpm_role_group` VALUES ('8', 'pig:roleGroup:testCompany:financeStaff', '总公司财务部职员', '', 'finance', '1', '1', 'pig', '', '1', null, '', '2020-06-24 11:26:58', '2020-06-24 13:52:08');
INSERT INTO `bpm_role_group` VALUES ('9', 'pig:roleGroup:testCompany:staff', '总公司职员', '', '', '1', '1.0.1', 'pig', '', '1', null, '', '2020-06-24 11:28:07', '2020-06-24 13:52:10');
INSERT INTO `bpm_role_group` VALUES ('10', 'pig:roleGroup:testCompany:director', '总公司部门负责人', '', '', '1', '1.0.1', 'pig', '', '1', null, '', '2020-06-24 11:28:44', '2020-06-24 13:52:12');
INSERT INTO `bpm_role_group` VALUES ('11', 'pig:roleGroup:gdBranchCompany:techDirector', '广分技术部负责人', '', 'tech', '1.0.1', '1', 'pig', '', '1', null, '', '2020-06-24 11:08:48', '2020-06-24 13:52:13');
INSERT INTO `bpm_role_group` VALUES ('12', 'pig:roleGroup:gdBranchCompany:techStaff', '广分技术部职员', '', 'tech', '1.0.1', '1', 'pig', '', '1', null, '', '2020-06-24 11:12:43', '2020-06-24 13:52:17');
INSERT INTO `bpm_role_group` VALUES ('13', 'pig:roleGroup:gdBranchCompany:generalManager', '广分总经理', '', 'manager', '1.0.1', '1', 'pig', '', '1', null, '', '2020-06-24 11:23:22', '2020-06-24 13:52:18');
INSERT INTO `bpm_role_group` VALUES ('14', 'pig:roleGroup:gdBranchCompany:financeDirector', '广分财务部负责人', '', 'finance', '1.0.1', '1', 'pig', '', '1', null, '', '2020-06-24 11:25:21', '2020-06-24 13:52:21');
INSERT INTO `bpm_role_group` VALUES ('15', 'pig:roleGroup:gdBranchCompany:financeStaff', '广分财务部职员', '', 'finance', '1.0.1', '1', 'pig', '', '1', null, '', '2020-06-24 11:26:58', '2020-06-24 13:52:22');
INSERT INTO `bpm_role_group` VALUES ('16', 'pig:roleGroup:gdBranchCompany:staff', '分公司职员', '', '', '1.0.1', '1.0.1', 'pig', '', '1', null, '', '2020-06-24 11:28:07', '2020-06-24 14:24:27');
INSERT INTO `bpm_role_group` VALUES ('17', 'pig:roleGroup:gdBranchCompany:director', '分公司部门负责人', '', '', '1.0.1', '1.0.1', 'pig', '', '1', null, '', '2020-06-24 11:28:44', '2020-06-24 14:24:44');

-- ----------------------------
-- Table structure for bpm_role_group_to_role
-- ----------------------------
DROP TABLE IF EXISTS `bpm_role_group_to_role`;
CREATE TABLE `bpm_role_group_to_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `role_group_id` bigint(20) unsigned DEFAULT NULL COMMENT '角色组编号',
  `role_id` bigint(20) unsigned DEFAULT NULL COMMENT '角色编号',
  `tenant_id` varchar(128) DEFAULT '' COMMENT '租户编号',
  `remarks` varchar(255) DEFAULT '' COMMENT '备注',
  `valid_state` tinyint(3) unsigned DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` bigint(20) DEFAULT NULL,
  `operator_name` varchar(255) DEFAULT '' COMMENT '操作人姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_tenant_id` (`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bpm_role_group_to_role
-- ----------------------------
INSERT INTO `bpm_role_group_to_role` VALUES ('1', '1', '1.0.1', 'pig', '', '1', null, '', '2020-06-24 14:16:33', '2020-06-24 14:16:33');
INSERT INTO `bpm_role_group_to_role` VALUES ('1.0.1', '1.0.1', '3', 'pig', '', '1', null, '', '2020-06-24 14:16:55', '2020-06-24 14:17:12');
INSERT INTO `bpm_role_group_to_role` VALUES ('3', '3', '4', 'pig', '', '1', null, '', '2020-06-24 14:17:09', '2020-06-24 14:17:15');
INSERT INTO `bpm_role_group_to_role` VALUES ('4', '10', '1.0.1', 'pig', '', '1', null, '', '2020-06-24 14:17:52', '2020-06-24 14:17:52');
INSERT INTO `bpm_role_group_to_role` VALUES ('5', '9', '3', 'pig', '', '1', null, '', '2020-06-24 14:18:38', '2020-06-24 14:18:38');
INSERT INTO `bpm_role_group_to_role` VALUES ('6', '9', '4', 'pig', '', '1', null, '', '2020-06-24 14:18:46', '2020-06-24 14:18:46');
INSERT INTO `bpm_role_group_to_role` VALUES ('7', '4', '5', 'pig', '', '1', null, '', '2020-06-24 14:19:35', '2020-06-24 14:19:40');
INSERT INTO `bpm_role_group_to_role` VALUES ('8', '5', '6', 'pig', '', '1', null, '', '2020-06-24 14:20:18', '2020-06-24 14:20:18');
INSERT INTO `bpm_role_group_to_role` VALUES ('9', '6', '7', 'pig', '', '1', null, '', '2020-06-24 14:21:13', '2020-06-24 14:21:33');
INSERT INTO `bpm_role_group_to_role` VALUES ('10', '7', '8', 'pig', '', '1', null, '', '2020-06-24 14:21:23', '2020-06-24 14:21:33');
INSERT INTO `bpm_role_group_to_role` VALUES ('11', '8', '9', 'pig', '', '1', null, '', '2020-06-24 14:21:29', '2020-06-24 14:21:34');
INSERT INTO `bpm_role_group_to_role` VALUES ('12', '10', '7', 'pig', '', '1', null, '', '2020-06-24 14:21:47', '2020-06-24 14:22:02');
INSERT INTO `bpm_role_group_to_role` VALUES ('13', '9', '8', 'pig', '', '1', null, '', '2020-06-24 14:21:52', '2020-06-24 14:22:03');
INSERT INTO `bpm_role_group_to_role` VALUES ('14', '9', '9', 'pig', '', '1', null, '', '2020-06-24 14:21:55', '2020-06-24 14:22:04');
INSERT INTO `bpm_role_group_to_role` VALUES ('15', '13', '10', 'pig', '', '1', null, '', '2020-06-24 14:24:00', '2020-06-24 14:24:00');
INSERT INTO `bpm_role_group_to_role` VALUES ('16', '11', '11', 'pig', '', '1', null, '', '2020-06-24 14:25:53', '2020-06-24 14:25:53');
INSERT INTO `bpm_role_group_to_role` VALUES ('17', '12', '12', 'pig', '', '1', null, '', '2020-06-24 14:26:19', '2020-06-24 14:26:19');
INSERT INTO `bpm_role_group_to_role` VALUES ('18', '17', '11', 'pig', '', '1', null, '', '2020-06-24 14:26:31', '2020-06-24 14:26:31');
INSERT INTO `bpm_role_group_to_role` VALUES ('19', '16', '12', 'pig', '', '1', null, '', '2020-06-24 14:26:40', '2020-06-24 14:26:40');
INSERT INTO `bpm_role_group_to_role` VALUES ('20', '14', '13', 'pig', '', '1', null, '', '2020-06-24 14:26:58', '2020-06-24 14:27:08');
INSERT INTO `bpm_role_group_to_role` VALUES ('21', '15', '14', 'pig', '', '1', null, '', '2020-06-24 14:27:06', '2020-06-24 14:27:11');
INSERT INTO `bpm_role_group_to_role` VALUES ('22', '17', '13', 'pig', '', '1', null, '', '2020-06-24 14:27:32', '2020-06-24 14:27:32');
INSERT INTO `bpm_role_group_to_role` VALUES ('23', '16', '14', 'pig', '', '1', null, '', '2020-06-24 14:27:43', '2020-06-24 14:27:43');

-- ----------------------------
-- Table structure for bpm_user
-- ----------------------------
DROP TABLE IF EXISTS `bpm_user`;
CREATE TABLE `bpm_user` (
  `id` varchar(32) NOT NULL,
  `user_id` bigint(20) NOT NULL COMMENT '用户编号',
  `user_name` varchar(128) NOT NULL DEFAULT '' COMMENT '用户账户',
  `real_name` varchar(128) NOT NULL DEFAULT '' COMMENT '用户名称',
  `email` varchar(255) NOT NULL COMMENT '邮箱',
  `phone` varchar(32) DEFAULT NULL COMMENT '联系方式',
  `position_code` varchar(64) NOT NULL DEFAULT '' COMMENT '岗位编码',
  `position_name` varchar(64) NOT NULL DEFAULT '' COMMENT '岗位名称',
  `gender` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '性别，0表示未知，1表示男，2表示女',
  `avatar` varchar(255) DEFAULT '' COMMENT '头像图片链接',
  `birth_date` datetime DEFAULT NULL COMMENT '生日',
  `company_id` bigint(20) DEFAULT '0' COMMENT '公司编号',
  `dept_id` bigint(20) DEFAULT '0' COMMENT '部门编号',
  `tenant_id` varchar(100) DEFAULT '' COMMENT '当前用户活动租户编号',
  `password` varchar(64) NOT NULL DEFAULT '' COMMENT '密码',
  `entry_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '入职时间',
  `leave_time` datetime DEFAULT NULL COMMENT '离职时间',
  `hire_status` tinyint(2) DEFAULT '1' COMMENT '雇佣状态 1 在职 1.0.1 离职',
  `valid_state` tinyint(2) DEFAULT '1' COMMENT '有效状态；0表示无效，1表示有效',
  `operator_id` bigint(20) unsigned DEFAULT '0' COMMENT '操作人工号',
  `operator_name` varchar(255) DEFAULT '' COMMENT '操作人姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_user_name` (`user_name`,`email`,`phone`),
  KEY `idx_tenant` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bpm_user
-- ----------------------------
INSERT INTO `bpm_user` VALUES ('1262383598905909250', '1.0.1', 'admin', '管理员', 'add@email.com', null, '', '', '0', 'http://wework.qpic.cn/bizmail/xpuWj7ibdYjibN5exHJww3QEoenrnnjfpuQhiaMLzeZsD4dNWIEC4af2g/0', null, '1', '1', 'pig', '$2a$10$z9oouf75V6yCozTR/PFdFuA90EEBcNlxDBBJOwmItHWFknst0MTl2', '2020-05-18 14:04:40', null, '1', '1', '0', '', '2020-05-18 14:04:40', '2020-06-24 12:48:07');
INSERT INTO `bpm_user` VALUES ('1262383598905909251', '3', 'hairman', '测试集团董事长', 'add@email.com', null, '', '', '0', 'http://wework.qpic.cn/bizmail/xpuWj7ibdYjibN5exHJww3QEoenrnnjfpuQhiaMLzeZsD4dNWIEC4af2g/0', null, '1', '1.0.1', 'pig', '$2a$10$z9oouf75V6yCozTR/PFdFuA90EEBcNlxDBBJOwmItHWFknst0MTl2', '2020-05-18 14:04:40', null, '1', '1', '0', '', '2020-05-18 14:04:40', '2020-06-24 13:59:50');
INSERT INTO `bpm_user` VALUES ('1262383598905909252', '4', 'generalManager', '测试集团总经理', 'add@email.com', null, '', '', '0', 'http://wework.qpic.cn/bizmail/xpuWj7ibdYjibN5exHJww3QEoenrnnjfpuQhiaMLzeZsD4dNWIEC4af2g/0', null, '1', '1.0.1', 'pig', '$2a$10$z9oouf75V6yCozTR/PFdFuA90EEBcNlxDBBJOwmItHWFknst0MTl2', '2020-05-18 14:04:40', null, '1', '1', '0', '', '2020-05-18 14:04:40', '2020-06-24 14:00:04');
INSERT INTO `bpm_user` VALUES ('1262383598905909253', '5', 'techDirector', '总公司技术部负责人', 'add@email.com', null, '', '', '0', 'http://wework.qpic.cn/bizmail/xpuWj7ibdYjibN5exHJww3QEoenrnnjfpuQhiaMLzeZsD4dNWIEC4af2g/0', null, '1', '1', 'pig', '$2a$10$z9oouf75V6yCozTR/PFdFuA90EEBcNlxDBBJOwmItHWFknst0MTl2', '2020-05-18 14:04:40', null, '1', '1', '0', '', '2020-05-18 14:04:40', '2020-06-24 12:48:07');
INSERT INTO `bpm_user` VALUES ('1262383598905909254', '6', 'techStaff', '总公司技术部职员', 'add@email.com', null, '', '', '0', 'http://wework.qpic.cn/bizmail/xpuWj7ibdYjibN5exHJww3QEoenrnnjfpuQhiaMLzeZsD4dNWIEC4af2g/0', null, '1', '1', 'pig', '$2a$10$z9oouf75V6yCozTR/PFdFuA90EEBcNlxDBBJOwmItHWFknst0MTl2', '2020-05-18 14:04:40', null, '1', '1', '0', '', '2020-05-18 14:04:40', '2020-06-24 12:48:07');
INSERT INTO `bpm_user` VALUES ('1262383598905909255', '7', 'techManager', '总公司技术部经理', 'add@email.com', null, '', '', '0', 'http://wework.qpic.cn/bizmail/xpuWj7ibdYjibN5exHJww3QEoenrnnjfpuQhiaMLzeZsD4dNWIEC4af2g/0', null, '1', '7', 'pig', '$2a$10$z9oouf75V6yCozTR/PFdFuA90EEBcNlxDBBJOwmItHWFknst0MTl2', '2020-05-18 14:04:40', null, '1', '1', '0', '', '2020-05-18 14:04:40', '2020-07-31 11:04:17');
INSERT INTO `bpm_user` VALUES ('1262383598905909256', '8', 'financeDirector', '总公司财务部负责人', 'add@email.com', null, '', '', '0', 'http://wework.qpic.cn/bizmail/xpuWj7ibdYjibN5exHJww3QEoenrnnjfpuQhiaMLzeZsD4dNWIEC4af2g/0', null, '1', '3', 'pig', '$2a$10$z9oouf75V6yCozTR/PFdFuA90EEBcNlxDBBJOwmItHWFknst0MTl2', '2020-05-18 14:04:40', null, '1', '1', '0', '', '2020-05-18 14:04:40', '2020-06-24 14:01:38');
INSERT INTO `bpm_user` VALUES ('1262383598905909257', '9', 'financeManager', '总公司财务部经理', 'add@email.com', null, '', '', '0', 'http://wework.qpic.cn/bizmail/xpuWj7ibdYjibN5exHJww3QEoenrnnjfpuQhiaMLzeZsD4dNWIEC4af2g/0', null, '1', '3', 'pig', '$2a$10$z9oouf75V6yCozTR/PFdFuA90EEBcNlxDBBJOwmItHWFknst0MTl2', '2020-05-18 14:04:40', null, '1', '1', '0', '', '2020-05-18 14:04:40', '2020-06-24 14:01:36');
INSERT INTO `bpm_user` VALUES ('1262383598905909258', '10', 'financeStaff', '总公司财务部职员', 'add@email.com', null, '', '', '0', 'http://wework.qpic.cn/bizmail/xpuWj7ibdYjibN5exHJww3QEoenrnnjfpuQhiaMLzeZsD4dNWIEC4af2g/0', null, '1', '3', 'pig', '$2a$10$z9oouf75V6yCozTR/PFdFuA90EEBcNlxDBBJOwmItHWFknst0MTl2', '2020-05-18 14:04:40', null, '1', '1', '0', '', '2020-05-18 14:04:40', '2020-06-24 14:01:36');
INSERT INTO `bpm_user` VALUES ('1262383598905909259', '11', 'gdBranchTechStaff', '广分技术部职员', 'add@email.com', null, '', '', '0', 'http://wework.qpic.cn/bizmail/xpuWj7ibdYjibN5exHJww3QEoenrnnjfpuQhiaMLzeZsD4dNWIEC4af2g/0', null, '4', '4', 'pig', '$2a$10$z9oouf75V6yCozTR/PFdFuA90EEBcNlxDBBJOwmItHWFknst0MTl2', '2020-05-18 14:04:40', null, '1', '1', '0', '', '2020-05-18 14:04:40', '2020-06-24 14:00:23');
INSERT INTO `bpm_user` VALUES ('1262383598905909260', '12', 'gdBranchTechDirector', '广分技术部负责人', 'add@email.com', null, '', '', '0', 'http://wework.qpic.cn/bizmail/xpuWj7ibdYjibN5exHJww3QEoenrnnjfpuQhiaMLzeZsD4dNWIEC4af2g/0', null, '4', '4', 'pig', '$2a$10$z9oouf75V6yCozTR/PFdFuA90EEBcNlxDBBJOwmItHWFknst0MTl2', '2020-05-18 14:04:40', null, '1', '1', '0', '', '2020-05-18 14:04:40', '2020-06-24 14:00:24');
INSERT INTO `bpm_user` VALUES ('1262383598905909261', '13', 'gdBranchGeneralManager', '广分总经理', 'add@email.com', null, '', '', '0', 'http://wework.qpic.cn/bizmail/xpuWj7ibdYjibN5exHJww3QEoenrnnjfpuQhiaMLzeZsD4dNWIEC4af2g/0', null, '4', '5', 'pig', '$2a$10$z9oouf75V6yCozTR/PFdFuA90EEBcNlxDBBJOwmItHWFknst0MTl2', '2020-05-18 14:04:40', null, '1', '1', '0', '', '2020-05-18 14:04:40', '2020-06-24 14:01:16');
INSERT INTO `bpm_user` VALUES ('1262383598905909262', '14', 'gdBranchFinanceDirector', '广分财务部负责人', 'add@email.com', null, '', '', '0', 'http://wework.qpic.cn/bizmail/xpuWj7ibdYjibN5exHJww3QEoenrnnjfpuQhiaMLzeZsD4dNWIEC4af2g/0', null, '4', '6', 'pig', '$2a$10$z9oouf75V6yCozTR/PFdFuA90EEBcNlxDBBJOwmItHWFknst0MTl2', '2020-05-18 14:04:40', null, '1', '1', '0', '', '2020-05-18 14:04:40', '2020-06-24 14:01:18');

-- ----------------------------
-- Table structure for bpm_user_role
-- ----------------------------
DROP TABLE IF EXISTS `bpm_user_role`;
CREATE TABLE `bpm_user_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(20) unsigned DEFAULT NULL COMMENT '用户工号',
  `role_id` bigint(20) unsigned DEFAULT NULL COMMENT '角色编号',
  `alias_name` varchar(255) DEFAULT NULL COMMENT '用户角色 别名',
  `tenant_id` varchar(128) DEFAULT '' COMMENT '租户编号',
  `remarks` varchar(255) DEFAULT '' COMMENT '备注',
  `valid_state` tinyint(3) unsigned DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` bigint(20) unsigned DEFAULT NULL COMMENT '操作人工号',
  `operator_name` varchar(255) DEFAULT '' COMMENT '操作人姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_tenant_id` (`tenant_id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bpm_user_role
-- ----------------------------
INSERT INTO `bpm_user_role` VALUES ('1', '1.0.1', '1', null, 'pig', '', '1', null, '', '2020-06-14 06:48:22', '2020-06-14 10:59:55');

-- ----------------------------
-- Table structure for bpm_user_task
-- ----------------------------
DROP TABLE IF EXISTS `bpm_user_task`;
CREATE TABLE `bpm_user_task` (
  `task_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `act_task_id` varchar(64) DEFAULT '' COMMENT 'flowable 任务编号',
  `apply_id` bigint(20) unsigned DEFAULT NULL COMMENT '申请编号',
  `process_id` bigint(20) DEFAULT '0' COMMENT '流程编号',
  `proc_inst_id` varchar(64) DEFAULT '' COMMENT '流程实例编号',
  `task_type` varchar(64) DEFAULT '' COMMENT '用户类型',
  `tenant_id` varchar(128) DEFAULT '' COMMENT '所属租户',
  `task_name` varchar(255) DEFAULT '' COMMENT '任务名称',
  `task_node_code` varchar(64) DEFAULT '' COMMENT '任务对应节点编号',
  `parent_task_id` bigint(20) unsigned DEFAULT NULL COMMENT '父级任务编号',
  `task_status` tinyint(5) DEFAULT '1' COMMENT '任务状态 1  表示未认领 1.0.1 表示已认领 3 表示已完成 4表示已取消  5:找不到人员系统自动完成任务',
  `role_group_code` bigint(20) unsigned DEFAULT NULL COMMENT '角色组编号',
  `role_code` bigint(20) unsigned DEFAULT NULL COMMENT '角色编号',
  `role_name` varchar(255) DEFAULT '' COMMENT '角色名称',
  `task_owner_user_id` bigint(20) unsigned DEFAULT NULL COMMENT '任务归属人工号',
  `task_owner_real_name` varchar(255) DEFAULT '' COMMENT '任务归属人姓名',
  `task_assignee_user_id` bigint(20) unsigned DEFAULT NULL COMMENT '任务处理人工号',
  `task_assignee_real_name` varchar(255) DEFAULT '' COMMENT '任务处理人姓名',
  `task_priority` tinyint(3) DEFAULT '10' COMMENT '任务优先等级',
  `form_key` varchar(64) DEFAULT '' COMMENT '表单关联KEY',
  `claim_time` datetime DEFAULT NULL COMMENT '任务认领时间',
  `due_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '任务到期时间',
  `approve_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '任务审批时间',
  `system` varchar(64) DEFAULT '' COMMENT '来源系统',
  `paltform` varchar(64) DEFAULT '' COMMENT '来源平台',
  `remarks` varchar(255) DEFAULT '' COMMENT '任务备注',
  `valid_state` tinyint(3) DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` bigint(20) DEFAULT NULL COMMENT 'operator_id',
  `operator_name` varchar(255) DEFAULT '' COMMENT '操作人工姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`task_id`),
  KEY `idx_apply_id` (`apply_id`),
  KEY `idx_proc_inst_id` (`proc_inst_id`),
  KEY `idx_process_id` (`process_id`),
  KEY `idx_tenant_id` (`tenant_id`),
  KEY `idx_task_status` (`task_status`),
  KEY `idx_user_id` (`task_owner_user_id`,`task_assignee_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


-- ----------------------------
-- Table structure for bpm_variable_dict
-- ----------------------------
DROP TABLE IF EXISTS `bpm_variable_dict`;
CREATE TABLE `bpm_variable_dict` (
  `variable_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号 ',
  `process_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '流程编号',
  `tenant_id` varchar(128) NOT NULL DEFAULT '' COMMENT '租户编号',
  `data_key` varchar(255) NOT NULL DEFAULT '' COMMENT '字段英文名称',
  `data_name` varchar(255) NOT NULL DEFAULT '' COMMENT '字段中文名称',
  `data_type` varchar(255) NOT NULL DEFAULT '' COMMENT '数据类型',
  `check_rule` varchar(255) DEFAULT '' COMMENT '校验规则（填写JUEL表达式）',
  `special_value` varchar(255) DEFAULT '' COMMENT '特殊值1',
  `special_value2` varchar(255) DEFAULT '' COMMENT '特殊值2',
  `process_data` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1 流程变量 0 非流程变量',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT '排序值',
  `allow_edit_node_id` varchar(255) DEFAULT '' COMMENT '允许编辑节点 默认发起人可以编辑字段',
  `hidden_node_id` varchar(255) DEFAULT '' COMMENT '不允许读取字段 默认所有节点都可以读取',
  `required_node_id` varchar(255) DEFAULT '' COMMENT '哪些节点当前字段必须传',
  `remarks` varchar(255) DEFAULT '' COMMENT '备注',
  `valid_state` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '状态 1 有效 0 失效',
  `operator_id` bigint(20) unsigned DEFAULT '0' COMMENT '操作人工号',
  `operator_name` varchar(128) DEFAULT '' COMMENT '操作人姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`variable_id`),
  KEY `idx_tenant_id` (`tenant_id`),
  KEY `idx_process_id` (`process_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='变量表';


-- ----------------------------
-- Table structure for bpm_tenant
-- ----------------------------
DROP TABLE IF EXISTS `bpm_tenant`;
CREATE TABLE `bpm_tenant` (
  `tenant_id` varchar(64) NOT NULL COMMENT '租户编号',
  `tenant_name` varchar(255) NOT NULL DEFAULT '' COMMENT '租户名称',
  `effective_start_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '有效期开始时间',
  `effective_end_time` datetime DEFAULT NULL COMMENT '有效期结束时间',
  `remarks` varchar(255) NOT NULL COMMENT '备注',
  `valid_state` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '有效状态；0表示无效，1表示有效',
  `operator_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '操作人工号',
  `operator_name` varchar(255) NOT NULL DEFAULT '' COMMENT '操作人姓名',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='租户表';
SET FOREIGN_KEY_CHECKS=1;
