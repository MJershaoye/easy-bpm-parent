package com.pig.easy.bpm.service.impl;

import com.pig.easy.bpm.dto.request.CompleteTaskDTO;
import com.pig.easy.bpm.service.UserTaskService;
import com.pig.easy.bpm.utils.Result;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.annotation.Reference;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.HashMap;

@SpringBootTest
class UserTaskServiceImplTest {

    @Autowired
    UserTaskService userTaskService;

    @Test
    void batchCompleteTask() {

        CompleteTaskDTO completeTaskDTO = new CompleteTaskDTO();
        completeTaskDTO.setBusinessData(new HashMap<>());
        completeTaskDTO.setTenantId("pig");
        completeTaskDTO.setApproveId(2L);
        completeTaskDTO.setApproveName("管理员");
        completeTaskDTO.setApproveOpinion("测试批量审批");
        completeTaskDTO.setApproveActionCode("approvePass");
        completeTaskDTO.setTaskIds(Arrays.asList(33L,34L));
        Result<Boolean> result = userTaskService.batchCompleteTask(completeTaskDTO);
        Assert.isNull(result.getData());
    }
}