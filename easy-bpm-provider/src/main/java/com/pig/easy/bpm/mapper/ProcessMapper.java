package com.pig.easy.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig.easy.bpm.dto.response.ProcessInfoDTO;
import com.pig.easy.bpm.entity.ProcessDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 流程表 Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-05-20
 */
@Mapper
public interface ProcessMapper extends BaseMapper<ProcessDO> {

    List<ProcessInfoDTO> getListByCondition(ProcessDO process);

    Integer updateProcessByProcessKey(ProcessDO process);
}
