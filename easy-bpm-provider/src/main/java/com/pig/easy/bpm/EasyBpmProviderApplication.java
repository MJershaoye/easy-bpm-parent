package com.pig.easy.bpm;


import com.alibaba.nacos.spring.context.annotation.config.EnableNacosConfig;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import com.alibaba.nacos.spring.context.annotation.discovery.EnableNacosDiscovery;
import com.pig.easy.bpm.dubbo.annotation.EnableBpmDubbo;
import com.pig.easy.bpm.mysql.annotation.EnableMysql;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.concurrent.CountDownLatch;

@EnableNacosConfig
@EnableNacosDiscovery
@EnableBpmDubbo(scanBasePackages = {"com.pig"})
@EnableTransactionManagement
@EnableAsync
@EnableMysql(scanBasePackages = "com.pig.easy.bpm.mapper*")
@NacosPropertySource(dataId = "easy-bpm-provider.properties",groupId = "DEFAULT_GROUP", autoRefreshed = true)
@SpringBootApplication(scanBasePackages = "com.pig.easy")
public class EasyBpmProviderApplication {

    public static void main(String[] args) throws InterruptedException {
        new SpringApplicationBuilder(EasyBpmProviderApplication.class).web(WebApplicationType.NONE).run(args);
        CountDownLatch count = new CountDownLatch(1);
        count.await();
    }

}
