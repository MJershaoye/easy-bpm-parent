package com.pig.easy.bpm.mapper;

import com.pig.easy.bpm.entity.TenantDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.pig.easy.bpm.dto.request.*;
import com.pig.easy.bpm.dto.response.*;
import java.util.List;

/**
 * <p>
 * 租户表 Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-09-02
 */
@Mapper
public interface TenantMapper extends BaseMapper<TenantDO> {

        List<TenantDTO> getListByCondition(TenantQueryDTO param);
}
